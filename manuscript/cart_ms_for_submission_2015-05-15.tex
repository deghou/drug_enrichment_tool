\documentclass{bioinfo}
\copyrightyear{2015}
\pubyear{2015}

\begin{document}
\firstpage{1}

\title[CART]{CART -- a chemical annotation retrieval toolkit}
\author[Deghou, Zeller, Iskar \textit{et~al}]{
  Samy Deghou,\!\textsuperscript{1,\ddag{}}
  Georg Zeller,\!\textsuperscript{1,\ddag{}}
  Murat Iskar,\!\textsuperscript{1,\ddag{}}
  Marja Driessen,\!\textsuperscript{1}
  Mercedes Castillo,\!\textsuperscript{1}
  Vera van Noort,\!\textsuperscript{1,2} and Peer Bork\textsuperscript{1,3,$\ast$}}
\address{
$^{1}$\mbox{ Structural and Computational Biology Unit, European Molecular Biology Laboratory, Heidelberg, Germany}\\
$^{2}$ Centre of Microbial and Plant Genetics, KU Leuven, Leuven,
Belgium\\ % TODO Vera, please check!
$^{3}$ Max Delbr{\"u}ck Centre for Molecular Medicine, Berlin, Germany}

\history{Received on XXXXX; revised on XXXXX; accepted on XXXXX}

\editor{Associate Editor: XXXXXXX}

\maketitle
\renewcommand*{\thefootnote}{\arabic{footnote}}
\newcommand{\myhref}[2]{\href{#1}{\texttt{#2}}}

\renewcommand*{\thefootnote}{\fnsymbol{footnote}}
\footnotetext[1]{to whom correspondence should be addressed: \myhref{mailto:bork@embl.de}{bork@embl.de}.}
\footnotetext[3]{these authors contributed equally.\vspace*{-4ex}}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% Word limit is ~1000 words (for application notes with a display item) %%%
%%% Please keep this in mind when editing!                                                %%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


\begin{abstract}

\section{Motivation:}
Data on bioactivities of drug-like chemicals is rapidly accumulating
in public repositories, creating new opportunities for research in
computational systems pharmacology. However, integrative analysis of
these data sets is difficult due to prevailing ambiguity between
chemical names and identifiers and a lack of cross-references between
databases.

\section{Results:}
To address this challenge, we have developed CART, a \textbf{C}hemical
\textbf{A}nnotation \textbf{R}etrieval \textbf{T}oolkit. As a key
functionality, it matches an input list of chemical names into a
comprehensive reference space to assign unambiguous chemical
identifiers. In this unified space, bioactivity annotations can be
easily retrieved from databases covering a wide variety of chemical
effects on biological systems. Subsequently, CART can determine
annotations enriched in the input set of chemicals and display these
in tabular format and network visualizations, thereby greatly
facilitating integrative analysis of chemical bioactivity data.

\section{Availability:}
CART is available as a Galaxy web service
(\href{cart.embl.de}{cart.embl.de}) and as an easy-to-install open
source command line tool (\href{cart.embl.de}{cart.embl.de} and
\href{github.com/gezel/cart}{github.com/gezel/cart}).

\section{Contact:} \href{bork@embl.de}{bork@embl.de}
\end{abstract}

\section{Introduction}

Understanding the effects of chemicals, in particular small organic
molecules, on biological systems is fundamental to research in
pharmacology, toxicology, chemical biology and related
fields. Bioactivities of chemicals can be investigated at various
scales analyzing drug-associated readouts, such as protein
interactions, cellular phenotypes, toxicity or side
effects~\citep{Iskar_CITE}. Owing to the development of
high-throughput screening technologies, bioactivity data for large
chemical libraries has rapidly accumulated in recent years and is
increasingly becoming available in public repositories (see
Table~\ref{tab:01}). While this has created tremendous opportunities
for research that aims to integrate these heterogeneous data sets in
order to gain a better systemic understanding of chemical effects, in
practice such efforts are severely impeded by disparities in data
representation. In particular, unambiguous identification of chemicals
across databases can be difficult, because a myriad of synonyms and
trade names exist for many chemicals, and even controlled nomenclature
and structural descriptions are sometimes ambiguous, similar to the
problem of mapping between various gene, transcript and protein
nomenclatures, now overcome by many bioinformatics tools~\citep[among
others]{DAVID_CITE, REFSEQ_CITE, UNIPROT_CITE}. To address the
persisting need in chemoinformatics, we here present CART, a
\textbf{C}hemical \textbf{A}nnotation \textbf{R}etrieval
\textbf{T}oolkit. In solving the chemical name-matching problem, CART
aims at integrating bioactivity annotations across various databases
to provide functional annotation and enrichment analysis for
chemicals. Thereby CART can identify coherent functional themes,
analogous to gene ontology annotation tools, such as
DAVID~\citep{DAVID_CITE}. This makes CART useful, e.g.\ for the
automatic characterization of hits derived from chemical
screens~\citep[for instance]{Rihel_CITE}. Also in other contexts,
annotating chemicals with various biological effects is becoming an
important task, which has so far largely required expert manual
annotation, but can be greatly simplified by CART.


\begin{table}[b]
\setlength{\tabcolsep}{2pt}
\processtable{Chemical bioactivity databases available through CART.\label{tab:01}}
{
\begin{tabular}{llll}\toprule
Bioactivity & Database & Size$^a$ & Reference\\\midrule
{\scriptsize Molecular targets} & {\scriptsize STITCH} & {\scriptsize
  136,641 / 8,702} & {\scriptsize \href{stitch.embl.de}{stitch.embl.de}}\\
 & {\scriptsize TTD} & {\scriptsize 11,340 / 1,120} & {\scriptsize \href{bidd.nus.edu.sg/group/cjttd}{bidd.nus.edu.sg/group/cjttd}}\\
 & {\scriptsize DrugBank} &{\scriptsize 853 / 147} & {\scriptsize \href{www.drugbank.ca}{www.drugbank.ca}}\\
{\scriptsize Metabolization} & {\scriptsize DrugBank} & {\scriptsize
  396 / 64} & {\scriptsize \href{www.drugbank.ca}{www.drugbank.ca}}\\
{\scriptsize Therapeutic class.} & {\scriptsize ChEMBL} & {\scriptsize
  1,118 / 1,538} & {\scriptsize \href{www.ebi.ac.uk/chembl/ftc}{www.ebi.ac.uk/chembl/ftc}}\\
  & {\scriptsize ATC} & {\scriptsize 2,515 / 924} & {\scriptsize \href{www.whocc.no/atc}{www.whocc.no/atc}}\\
{\scriptsize Drug side effects} & {\scriptsize SIDER} & {\scriptsize
  995 / 4,192} & {\scriptsize \href{sider.embl.de}{sider.embl.de}}\\
{\scriptsize Toxicity} & {\scriptsize DrugMatrix} & {\scriptsize 742 /
  22} &  {\scriptsize \href{https://ntp.niehs.nih.gov/drugmatrix}{https://ntp.niehs.nih.gov/drugmatrix}}\\\botrule
\end{tabular}}{% table footnotes in here
$^a$ Number of annotated chemicals / annotation terms per database,
see also Suppl.\ Fig.~3.
}
\end{table}

\vspace*{-4ex}
\section{Approach}


%%% FIGURE 1
\begin{figure*}[t!]
\vspace*{-2ex}
\centerline{\includegraphics{Figure-1-v4}}
\vspace*{-2ex}
\caption{Typical CART workflow including chemical name matching,
  annotation retrieval and enrichment analysis. The lower panels
  contain a toy example of non-steroidal anti-inflammatory (NSAID)
  compounds and show excerpts of how these are matched and annotated
  by CART, the rightmost panel displays a (partial) enrichment
  network; PTGS -- prostaglandin-endoperoxide synthase targets, M01A
  -- ATC code for NSAIDs, Adj.\ P -- FDR-corrected P-value, nephritis
  and vasculitis are NSAID-associated side effects. See Suppl.\ Note~1
  and Suppl.\ Fig.~4 for an application of CART to hits from a drug
  screen.}
\label{fig:01}
\vspace*{-3ex}
\end{figure*}


% general description of CARTs functionality: name matching
The first component of CART consists of matching user-provided
chemical names to a comprehensive dictionary of synonyms, serving as a
reference space for disambiguation to unique chemical identifiers
(Figure~\ref{fig:01}). To improve matching sensitivity over exact
synonym look-up, we additionally implemented an approximate text
matching method based on the Apache Lucene search engine
(\href{http://lucene.apache.org/}{http://lucene.apache.org/}) and
heuristics such as the conversion between salt (e.g.\ salicylate) and
acid form (salicylic acid).

% general description of annotation retrieval and enrichment analysis
Mapping to this chemical reference space facilitates subsequent
retrieval of various annotations of bioactivities
(Table~\ref{tab:01}). Specifically, this allows for easy,
multi-facetted annotation of chemical libraries, synonym retrieval,
which is useful e.g.\ for text mining, and the identification of
bioactivities that are enriched in the user-provided
input. Statistical significance for these enrichments is established
using Fisher's exact test with FDR correction for multiple testing.

% use case
In a typical case, users may want to subject a set of hits resulting
from a high-throughput chemical screen to CART analysis. After name
matching, the enrichment analysis can be done relative to a
user-specified background, in this case the library of all chemicals
probed in the screen. Enriched annotations are subsequently retrieved
from databases describing chemical effects at various scales,
including molecular targets, metabolizing enzymes, functional
classifications, indication areas and side effects (Table 1). The
results are visualized as a network linking the input set of chemicals
to enriched annotations (Figure~\ref{fig:01}, Suppl.\ Note~1, Suppl.\
Fig.~4). This network can be interactively explored by the user as it
is based on the Cytoscape.web browser~\citep{CYTOSCAPEWEB_CITE}.

% Galaxy interface
The Galaxy~\citep{GALAXY_CITE} front-end of CART enables users to
combine individual modules into new workflows, allowing for easy
customization and extension of the standard use case described
above. Galaxy moreover facilitates reproducibility due to its history and
sharing functionalities~\citep{GALAXY_CITE}.

\vspace*{-4ex}
\section{Results}
% reference chemical space and matching speed
CART uses a comprehensive chemical reference space of about 167.6
million names and synonyms disambiguated to 33.38 million chemical
identifiers based on information from the STITCH database version
4.0~\citep{STITCH_CITE}. Matching user-provided chemical names into
this reference space is very fast and scales well, e.g.\ processing
1,000 chemicals takes $<$40 seconds (Suppl.\ Fig.~1), allowing
integrative analyses at a large scale. This is becoming crucial due to
the data deluge of publicly available chemical bioactivity
data~\citep{PUBCHEM_CITE}.

% name matching accuracy
As CART's chemical name matching algorithm is not based on structural
information, which is still considered best practice for chemical
identification, we sought to benchmark its accuracy. For this we used
four datasets for which a mapping to STITCH or PubChem identifiers
already existed and could serve as a gold standard for evaluations. We
found CART's sensitivity and precision to range between 91.1 and
98.6\% and between 80.0 and 98.4\%, respectively (Suppl.\ Fig.~2). As
an additional means of ensuring high analysis standards, CART enables
the user to interactively curate the automatic name matching results
before proceeding further.

% integrated data bases
Owing to its unified reference chemical space, CART offers seamless
integration of user-provided data with a number of databases
containing functional annotations of chemicals at various scales
(Table~\ref{tab:01}). These databases vary in scope, as the number of
annotated chemicals ranges from $>$130,000 compounds with known
protein interactions~\citep{STITCH_CITE, TTD_CITE} to a few hundred
drugs for which therapeutic classification, metabolization and
toxicity information~\citep{FTC_CITE, DRUGBANK_CITE, SIDER_CITE} is
publicly available (Suppl.\ Fig.~3). However, for a set of 836
well-characterized chemicals, annotations from $\geq$5 databases are
provided (Suppl.\ Fig.~3). CART's annotation and enrichment
functionality is demonstrated on drug sets previously defined in a
study~\citep{Rihel_CITE} that screened chemicals for behavioural
effects on zebrafish larvae (Suppl.\ Note~1 and Suppl.\ Fig.~4). It
revealed coherent themes of drug bioactivities, which could otherwise
only be discovered by expert manual annotations (as done
in~\cite{Rihel_CITE}).


\vspace*{-4ex}
\section{Conclusion}
%
CART implements a fast, scalable and accurate approach for matching
chemical names to a comprehensive chemical universe. This facilitates
the retrieval of enriched annotations from various databases
describing chemical effects on biological systems (Table~\ref{tab:01})
and their exploration in an interactive manner. CART thus makes
integrative analysis of chemical bioactivity data easy and accessible
to a broad non-specialist research community.


\vspace*{-4ex}
\section*{Acknowledgement}
We thank Yan Ping Yuan for technical support and members of the Bork group
for helpful discussion and testing.

%paragraph{Funding\textcolon} This work was supported by EMBL core funding.

\vspace*{-4ex}
\bibliographystyle{bioinformatics}
\begin{thebibliography}{} 
%
% uniprot
\bibitem[Apweiler {\it et~al}., 2004]{UNIPROT_CITE} Apweiler,R.,
  Bairoch,A., Wu,C.H., et al. (2004) UniProt: the Universal Protein
  knowledgebase. {\it Nucleic Acids Res}, {\bf 32}, D115-9.
%
% ChEMBL-FTC
\bibitem[Croset {\it et~al}., 2014] {FTC_CITE} Croset,S.,
  Overington,JP., Rebholz-Schuhmann,D. (2014) The fuctional
  therapeutic chemical classification system. {\it Bioinformatics},
  {\bf 30(6)}, 876-83.
%
% galaxy 
\bibitem[Goecks {\it et~al}., 2010]{GALAXY_CITE} Goecks,J.,
  Nekrutenko,A., Taylor,J. et al. (2010) Galaxy: a comprehensive
  approach for supporting accessible, reproducible, and transparent
  computational research in the life sciences. {\it Genome Biol}, {\bf
    11(8)}, R86.
%
% david
\bibitem[Huang {\it et~al}., 2009]{DAVID_CITE} Huang,da W.,
  Sherman,B.T., Lempicki,R.A. (2009) Systematic and integrative
  analysis of large gene lists using DAVID bioinformatics
  resources. {\it Nat Protoc}, {\bf 4(1)}, 44-57.
%
% ISKAR
\bibitem[Iskar {\it et~al}., 2012]{Iskar_CITE} Iskar,M., Zeller,G.,
  Zhao,X.M., et al. (2012) Drug discovery in the age of systems
  biology: the rise of computational approaches for data integration,
  {\it Curr Opin Biotechnol} {\bf 23(4)}, 609-16.
%
% SIDER
\bibitem[Kuhn {\it et~al}., 2010] {SIDER_CITE} Kuhn,M., Campillos,M.,
  Letunic,I., et al. (2010) A side effect resource to capture
  phenotypic effect of drugs. {\it Mol Syst Biol}, {\bf 6}:343.
%
% STITCH
\bibitem[Kuhn {\it et~al}., 2014]{STITCH_CITE} Kuhn,M., Szklarczyk,D.,
  Pletscher-Frankild S., et al. (2014), STITCH 4: integration of
  protein-chemical interactions with user data. {\it Nucleic Acids
    Res}, {\bf 42}, 401-7.
%
% DrugBank
\bibitem[Law {\it et~al}., 2014] {DRUGBANK_CITE} Law,V., Knox,C.,
  Djoumbou,Y. et al. (2014) DrugBank 4.0: shedding new light on drug
  metabolism. {\it Nucleic Acids Res}, {\bf 42}, 1091-7.
%
% cytoscape web
\bibitem[Lopes {\it et~al}., 2010]{CYTOSCAPEWEB_CITE} Lopes,C.T.,
  Franz,M., Kazi,F. et al. (2010) Cytoscape Web: an interactive
  web-based network browser. {\it Bioinformatics}, {\bf 26(18)},
  2347-8.
%
% refseq
\bibitem[Pruitt and Maglott, 2001]{REFSEQ_CITE} Pruitt,K.D. and
  Maglott,D.R. (2001) RefSeq and LocusLink: NCBI gene-centered
  resources. {\it Nucleic Acids Res}, {\bf 29}, 137-40.
%
% TTD
\bibitem[Qin {\it et~al}., 2014]{TTD_CITE} Qin,C., Zhang,C., Zhu,F.,
  et al. (2014) Therapeutic target database update 2014: a resource
  for targeted therapeutics. {\it Nucleic Acids Res}, {\bf 42}, 111-23.
%
% zebrafish screen
\bibitem[Rihel {\it et~al}., 2010] {Rihel_CITE} Rihel,J., Prober,
  D.A., Arvanites, A., et al. (2010) Zebrafish behavioral profiling
  links drugs to biological targets and rest/wake regulation. {\it Science},
  {\bf 327(5963)}, 348-51.
%
% pubchem 2
\bibitem[Wang {\it et~al}., 2012]{PUBCHEM_CITE} Wang Y, Xiao J, Suzek
  TO, et al. PubChem's BioAssay Database. {\it Nucleic Acids Res},
  D400-12.


\end{thebibliography}
\end{document}
