#!/bin/bash
SOFTWARE_NAME=cart
SOLR_VERSION=solr
SOFTWARE_URL=http://det.embl.de/static/cart.tar.gz
SOLR_INDEX_URL=https://vm-lux.embl.de/~deghou/data/drug-enrichment-tool/new-09.01.2015/${SOLR_VERSION}.tar.gz
DOWNLOAD_DIRECTORY=`pwd`
INSTALLATION_DIRECTORY=${HOME}
echo -n "Enter the absolute path of the desired installation directory and press [ENTER] (or type directly unter for an installation under ${HOME}: "
read INSTALLATION_DIRECTORY
if [ -z "$INSTALLATION_DIRECTORY" ]
then
INSTALLATION_DIRECTORY=${HOME}
fi
echo "CART will be installed here : ${INSTALLATION_DIRECTORY}"
INSTALLATION_DIRECTORY=${INSTALLATION_DIRECTORY}/drug-enrichment-tool
SOLR_INSTALLATION_DIRECTORY=${INSTALLATION_DIRECTORY}/solr/solr
DATABASES_DIRECTORY=${INSTALLATION_DIRECTORY}/${SOFTWARE_NAME}/databases
DESCRIPTION_FILE="${INSTALLATION_DIRECTORY}/${SOFTWARE_NAME}/static/all-drug-description_ver2.txt"
HEADER_JS="${INSTALLATION_DIRECTORY}/${SOFTWARE_NAME}/static/header_js.js"
FOOTER_JS="${INSTALLATION_DIRECTORY}/${SOFTWARE_NAME}/static/footer_js.js"
TEST_DIRECTORY=${INSTALLATION_DIRECTORY}/${SOFTWARE_NAME}/"test"
PYTHON_VIRTUALENV=${SOFTWARE_NAME}-virtualenv
######################################################
########### USER'S PYTHON VERSIONAS TO BE = 2.7  #####
######################################################
case "$(python --version 2>&1)" in
    *" 2.7"*)
        echo "Python version requirements satisfied!"
        ;;
    *)
        echo "Wrong Python version! Please install python 2.7"
	exit
        ;;
esac
######################################################
########### SET THE PYTHON ENVIRONMENTS    ###########
######################################################
require_module=false
require_module_pandas=false
require_module_matplotlib=false
require_module_scipy=false
require_module_numpy=false
require_module_statsmodels=false
require_module_psutil=false
require_module_networkx=false
pip_installed=false
virtualenv_installed=false
python -c 'import pandas' 2>/dev/null && require_module_pandas=false || require_module_pandas=true
python -c 'import matplotlib' 2>/dev/null && require_module_matplotlib=false || require_module_matplotlib=true
python -c 'import scipy' 2>/dev/null && require_module_scipy=false || require_module_scipy=true
python -c 'import psutil' 2>/dev/null && require_module_psutil=false || require_module_psutil=true
python -c 'import numpy' 2>/dev/null && require_module_numpy=false || require_module_numpy=true
python -c 'import statsmodels' 2>/dev/null && require_module_statsmodels=false || require_module_statsmodels=true
python -c 'import networkx' 2>/dev/null && require_module_networkx=false || require_module_networkx=true
if which pip; then
    pip_installed=true
fi
if which virtualenv; then
    virtualenv_installed=true
fi
if $required_module_pandas || $required_module_matplotlib || $required_module_scipy || $required_module_numpy || $required_module_statsmodels || $required_module_psutil || $required_module_networkx; then
    echo "Your python installation requires at least one of these modules : pandas, matplotlib, scipy, psutil, numpy, statsmodels, networkx"
    echo "Shall det install them for you (it might require the use of sudo, unless you have pip and virtualenv already installed) [y/n] ?"
    read sudo_authorization
    echo "caca"
    echo $sudo_authorization
    echo "pipi"
    if [[ $sudo_authorization == "y" ]]; then
        if ! $virtualenv_installed; then
                echo "first false"
            if ! $pip_installed; then
                echo "second false"
                sudo easy_install pip
            fi
            sudo easy_install virutalenv
        fi
        virtualenv -p `which python` ${HOME}/.${PYTHON_VIRTUALENV}/python-2.7 --system-site-packages
        source ${HOME}/.${PYTHON_VIRTUALENV}/python-2.7/bin/activate
        if $require_module_pandas; then
            pip install pandas
        fi
        if $require_module_matplotlib; then
            pip install matplotlib
        fi
        if $require_module_scipy; then
            pip install scipy
        fi
        if $require_module_numpy; then
            pip install numpy
        fi
        if $require_module_statsmodels; then
            pip install statsmodels
        fi
        if $require_module_psutil; then
            pip install psutil
        fi
        if $require_module_networkx; then
            pip install networkx
        fi
    else
        echo "Please install the required libraries before installing det"
        exit
    fi
else
 echo "no module required"
fi
######################################################
########### USER'S JAVA VERSION HAS TO BE > 1.5  #####
######################################################
if type -p java; then
    echo found java executable in PATH
    _java=java
elif [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]];  then
    echo found java executable in JAVA_HOME
    _java="$JAVA_HOME/bin/java"
else
    echo "It looks like java is not installed. Please install java (version 1.6 or higher) and re-run this script."
    exit
fi

if [[ "$_java" ]]; then
    version=$("$_java" -version 2>&1 | awk -F '"' '/version/ {print $2}')
    echo version "$version"
    if [[ "$version" > "1.5" ]]; then
        echo "Java version requirement is satisfied( > 1.5)"
    else
        echo "Java version is less than 1.5, please install a new java version before pursuing (java > 1.5 is required to run the name matching tool :-)"
	exit 
    fi
fi