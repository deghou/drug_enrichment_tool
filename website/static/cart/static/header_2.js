
	    	<!-- P value is log10(), which is changed with perl file as well.  -->
	    	<input style="width: 100px" class="input-range" type="range" id="pval" size=5 value="-2" min="-50" max="-2"> 10^(<span class="range-value"></span>) P value <br> 
 	   <input style="width: 100px" class="input-range2" type="range" id="odds" size=5 value="3" min="3" max="250">  <span class="range-value2"></span> Odds Ratio <br> 
 	   		<!-- button for subsetting the network.  -->
  	    	<button style="height:30px; width:90px" id="redraw-button">Redraw</button><br><br>

        
        
        
        <h2>Layout</h2>
			<!-- multiple layout algorithm names/options  -->
	    	<select id="layout-select">
	    		<option value="grid">Grid</option>
	    		<option value="cose" selected>Cose</option>
	    		<option value="cola">Cola</option>
	    		<option value="circle">Circle</option>
	    		<option value="breadthfirst">Breadth first</option>
	    		<option value="null">Null</option>
	    	</select>
	    	<!-- action button for layout  -->
	    	<button style="height:30px; width:90px" id="layout-button">Apply</button>

 </div>
</div>


<script id="jsbin-javascript">
<!-- ####################################################################################  -->

<!-- this part is just to print back the value of range inputs, on the right side.  -->

var range = $('.input-range'),
    value = $('.range-value');
var range2 = $('.input-range2'),
    value2 = $('.range-value2');

value.html(range.attr('value'));    
range.on('input', function(){
    value.html(this.value);
}); 
value2.html(range2.attr('value'));
range2.on('input', function(){
    value2.html(this.value);
}); 

<!-- end of this panel.  -->
<!-- ####################################################################################  -->


$(function(){ // on dom ready
var wholenetwork = {
    nodes: [

<!-- Here, we fill in the network usign perl/python. -->
