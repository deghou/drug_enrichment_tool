import pkg_resources
pkg_resources.require( "Cheetah" )

from Cheetah.Template import Template

def fill_template( template_text, context=None, **kwargs ):
    if not context:
        context = kwargs
    if '__datatypes_config__' in context:
        print context['__datatypes_config__']
    if 'heuristic' in context:
        print context['heuristic']
    print '\n\n'
    return str( Template( source=template_text, searchList=[context] ) )
