### specific to the name matching
# $1 INPUT foreground
# $2 INPUT background
# $3 INPUT fuzzy
# $4 INPUT heuristic
# $5 OUTPUT output-foreground
# $6 OUTPUT output-background
### specific to the enrichment
# $7 INPUT database-list
# $8 INPUT significance level
# $9 INPUT correction-test
# $10 INPUT foreground-enrichment
# $11 OUTPUT output-enr (nur 1 !!)
# $12 OUTPUT output-ann (nur 1 !!)
# $13 OUTPUT output_network_link
# $14 html output form visualization modules (table with links)
# $15 synonym output

echo $1
echo $2
echo $3
echo $4
echo $5
echo $6
echo $7
echo $8
echo $9
echo $10
echo $11
echo $12
echo $13
echo $14
echo $15

dbs=$7
dbs=(${dbs//,/ })
backg=$6
DET_DIRECTORY="/home/det/galaxy-dist-v2/tools/det"
NAME_MATCHING_SCRIPT=${DET_DIRECTORY}/src/name_matching.py
ENRICHMENT_SCRIPT=${DET_DIRECTORY}/src/enrichment_calculation.py
NETWORK_RENDERER_SCRIPT=${DET_DIRECTORY}/src/network_generator.py
LINKS_TABLE_GENERATOR=${DET_DIRECTORY}/src/result_annotator.py
APPEND_SCRIPT=${DET_DIRECTORY}/src/append-to-file.py
DESCRIPTION_FILE=${DET_DIRECTORY}/static/all-drug-description_ver2.txt
HEADER_HTML_JAVASCRIPT=${DET_DIRECTORY}/static/index1.html
FOOTER_HTML_JAVASCRIPT=${DET_DIRECTORY}/static/index2.html
NETWORK_HTML_PAGES_DIRECTORY=/home/deghou/public_html/static
URL="http://sam.embl.de/~deghou/static"
## MATCH FOREGROUND
python ${NAME_MATCHING_SCRIPT} -n $1 -o $5 -a $3 -e $4
## MATCH BACKGROUND IF ONE SELECTED
if [[ $2 == "None" ]]
    then
        echo "No background provided"
        backg='ALL'
    else
    echo "a"
        python ${NAME_MATCHING_SCRIPT} -n $2 -o $6 -a $3 -e $4
fi
## ENRICHMENT
echo -e 'property\tdatabase\tcorrected p value\tp value\todds ratio\tn_r' > ${11}
col_ent=`mktemp /tmp/XXXXXXXXXX`
tmp_ann=`mktemp /tmp/XXXXXXXXXX`
for i in "${!dbs[@]}"
    do
        db=${dbs[i]}
    ### create tmp files (one for the enrichment and one for the annotation)
        file_enr=`mktemp /tmp/XXXXXXXXXX`
        file_ann=`mktemp /tmp/XXXXXXXXXX`
    ### compute enrichment and write results of database to respective tmp files
    python ${ENRICHMENT_SCRIPT} -f $5 -b $6 -d $db -c $9 -a $8 -o $file_enr -p $file_ann
    ### append results to final output files to be returned to galaxy
        tail -n +2 "$file_enr" >> ${11}
        cut -f3 "$file_ann" > $col_ent
        paste ${12} $col_ent > $tmp_ann
        mv $tmp_ann ${12}
    done
cut -f1,2 "$file_ann" > $col_ent
paste $col_ent ${12}  > $tmp_ann
mv $tmp_ann ${12}
### generate the tables with external links
python ${LINKS_TABLE_GENERATOR} -i ${11} -o ${14}
### produce the network
file_network_html_page_tmp=`mktemp /tmp/XXXXXXXXXX`
network_html_page=${NETWORK_HTML_PAGES_DIRECTORY}/$(basename "$file_network_html_page_tmp").html
network_html_page_adress=${URL}/$(basename "$file_network_html_page_tmp").html
echo ${network_html_page}
python ${NETWORK_RENDERER_SCRIPT} -a ${12} -e ${11} -d ${DESCRIPTION_FILE} -k ${HEADER_HTML_JAVASCRIPT} -f ${FOOTER_HTML_JAVASCRIPT} -o ${network_html_page}
echo $network_html_page_adress
echo "<!doctype html><head></head><body><a href=\""$network_html_page_adress"\">Click here</a></body></html>" > ${13}
### synonyms
python ${NAME_MATCHING_SCRIPT} -n $1 -o ${15} -s true -a $3 -e $4
