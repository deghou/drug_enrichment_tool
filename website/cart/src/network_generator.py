import re
import math
import argparse
import os
import ConfigParser
# To change this license header, choose License Headers in Project Properties.
# To change this template file, choose Tools | Templates
# and open the template in the editor.


def generateTabFormatFromEnrichmentOutputs(annotation_file,description_file,enrichment_results_file,index1,index2,html_page_for_output):
    #build foreground dictionary (key chemical)
    foreground_annotation_map = {}
    cid_name_map = {}
    ##build the cid name mapping (@DICT)
    with open(annotation_file) as f:
        for line in f:
            el = line.split('\t')
            chemical_cid = el[0]
            chemical_name = el[1]
            if len(chemical_name) > 30:
                chemical_name = chemical_name[:30]
            term_databases = el[2:]
            # Brauch man nicht aber naja .. 
            if chemical_cid != "NA":
                cid_name_map[chemical_cid] = chemical_name
                for term_category in term_databases:
                    terms = term_category.split(";")
                    for t in terms:
                        t = t.strip()
                        if t != "NA" and len(t) >= 1:
                            if t in foreground_annotation_map:
                                foreground_annotation_map[t].append(chemical_cid)
                            else:
                                foreground_annotation_map[t] = [chemical_cid]
            else:
                print "ERROR : the CID column of the enrichment output contains a NA !!"
                            
#        print(cid_name_map)
        
    term_name_dict = {}
    term_link_dict = {}
    term_db_dict = {}
    ##build the term description map (@DICT)
    ##build term name map (@DICT)
    ##build term db map (@DICT)
    print(description_file)
    with open(description_file) as f:
        for line in f:
            el = line.split("\t")                
            term_id = el[0].lower().strip()
            db = el[1].strip()
            description = el[2].strip()
            html_link = el[3].strip()
            if db != "Indications":
                if len(description) > 30:
                    description = description[:30]
                description = description.split(";")[0]
                term_name_dict[term_id] = description
                term_link_dict[term_id] = html_link
                term_db_dict[term_id] = db
        #print term_name_dict
        #print term_link_dict
        #print term_db_dict
        
    ##Build the term p value map (@DICT)
    ##Build the termp odd ratios map (@DICT)
    term_p_value_dict = {}
    term_odd_ratios_dict = {}
    with open(enrichment_results_file) as f:
        next(f)
        for line in f:
            el = line.split("\t")
            term = el[0]
            db = el[1]
            p_value = el[2]
            odds = el[4]
            if p_value == "inf":
                p_value = 10000
            if odds == "inf":
                odds = 10000
            term_p_value_dict[term] = p_value
            term_odd_ratios_dict[term] = odds
#    print term_p_value_dict
#    print term_odd_ratios_dict
#    print term_db_dict

    ##Now create the final html page
    ####First load index1.html
    final_html_file = ""
    #print len(final_html_file)
    with open(index1, 'r') as content_file:
        content = content_file.read()
        final_html_file = final_html_file + content
        print len(final_html_file)
        ####Second load the enrichment terms information
        print 'assigning colors to terms ..'
        for term in term_p_value_dict:
    #        print term
            #if term_name_dict[term].lower() == "amenorrhoea":
            #    print "caca"
            db = term_db_dict[term]
            color="#A9A9F5";
            shape="ellipse";
            # @TODO toxicology teerms are not in the description file yet can appear as annotation and will remain with the color and shape of the target alike terms !!
            if db == "ATC" or db == "FTC":
                color="#D0F5A9";
                shape="rectangle";
            if db == "Side effect" or db == "Toxicity":
                color="#F79F81";
                shape="triangle";
            p_wert = str(math.log(float(term_p_value_dict[term]))/math.log(10))
            odd_wert = str(term_odd_ratios_dict[term])
            label = term_name_dict[term]
            if label == '':
                label = term
            final_html_file = final_html_file + "\n" + "{ data: { id: \'" + term + "\', label: \'" + label + "\', faveColor: \'" + color + "\', href:\'" + term_link_dict[term.lower()] + "\', faveShape: \'" + shape + "\', weight:30, db: \'" + term_db_dict[term] + "\', pval: " + p_wert + ", odds: " + odd_wert + " } },\n"
    ####Third load the chemical information
    print 'creating nodes and edges ..'
    all_cids = cid_name_map.keys()
    for cid in all_cids:
        final_html_file = final_html_file + "{ data: { id: \'" + cid +"\', label: \'" + cid_name_map[cid] + "\', faveColor: \'#848484\', href:\'http://stitch.embl.de/interactions/" + cid_name_map[cid].upper() + "?species=9606\', faveShape:\'roundrectangle\', weight:60, db:\'unfiltered\', pval: -10000, odds: 10000 } },\n"
    print len(final_html_file)
    final_html_file = final_html_file + "   ],\n    edges: [\n"
    print len(final_html_file)
    ####Fourth create the network
    network_tab = ""
    for term in term_p_value_dict:
        #########
        ######### CAREFUL !!! the term.upper() might be buggy !!
        #########
        chemicals = foreground_annotation_map[term]
        for cid in chemicals:
            network_tab = network_tab + "{ data: { source: \'" + term + "\', target: \'" + cid + "\' } },\n";
    network_tab = network_tab.strip()               
    final_html_file = final_html_file + '\n' + network_tab
    print len(final_html_file)
    ####Fifth load index1.html
    print 'printing footer to final html output ..'
    with open(index2, 'r') as content_file:
        content = content_file.read()
        final_html_file = final_html_file + content
    print 'printing final string to final html ..'
    f = open(html_page_for_output,'w')
    f.write(final_html_file)
    f.close()

                    
                    

if __name__ == '__main__': 
    #annotation_res_ex = "/Users/deghou/desktop/annotation-res.tab"
    #enrichment_res_ex = "/Users/deghou/desktop/enrichment-res.tab"
    #description_file_ex = "/Users/deghou/desktop/description-file.tab"
    #index1 = "/Users/deghou/Desktop/index1.html"
    #index2 = "/Users/deghou/Desktop/index2.html"
    parser = argparse.ArgumentParser(description='Generates a HTML page rendering an interactive network', version='0.1')
    parser.add_argument('-a', '--annotations', type=str, help='annotation file (enrichment output)')
    parser.add_argument('-e', '--enrichments', type=str, help='enrichment file (enrichment output)')
    parser.add_argument('-o', '--final_html_output', type=str, help='final html output') 
	#generateTabFormatFromEnrichmentOutputs(annotation_res_ex,description_file_ex, enrichment_res_ex,index1,index2)
    args = parser.parse_args()
    cfg_file = os.path.join(os.path.dirname(os.path.realpath(__file__)), '../conf/settings.cfg')
    cfg = ConfigParser.ConfigParser()
    cfg.read(cfg_file)
    description_file = cfg.get('visualization', 'description_file')
    header_js = cfg.get('visualization','header_js')
    footer_js = cfg.get('visualization','footer_js')
    generateTabFormatFromEnrichmentOutputs(args.annotations,description_file,args.enrichments,header_js,footer_js,args.final_html_output)
    
