from setuptools import setup

setup(name='drug enrichment tool',
      version='1.0',
      description='A tool to compute drug enrichment',
      author='Samy Deghou, Georg Zeller, Murat Iskar',
      author_email='deghou@embl.de, zeller@embl.de, iskar@embl.de',
      url='http://www.python.org/sigs/distutils-sig/',
      
      install_requires=[
         'matplotlib',
	 'psutil',
         'networkx'
      ]
     )
