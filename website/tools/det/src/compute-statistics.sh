#!/bin/bash

file=$1

no_reference_id_1=`grep "analyse:	false	NA" ${file} | wc -l`
no_reference_id_2=`grep "analyse:	true	NA" ${file} | wc -l`
echo $no_reference_id_1
no_reference_id=$((no_reference_id_1 + no_reference_id_2))
total_number_of_chemicals=`grep "analyse:" $file | grep -v "Inputed chemical" | wc -l`
total_number_of_chemicals_considered=$((total_number_of_chemicals - no_reference_id))

total_exact=`grep EXACT ${file} | grep -v "analyse:	false	NA" | wc -l` 
tp_exact=`grep EXACT ${file} | grep "analyse:	true	c" | wc -l`
fp_exact=`grep EXACT ${file} | grep "analyse:	false	c" | wc -l`

total_fuzzy=`grep FUZZY ${file} | grep -v "analyse:	false	NA" | wc -l`
tp_fuzzy=`grep FUZZY ${file} | grep "analyse:	true	c" | wc -l`
fp_fuzzy=`grep FUZZY ${file} | grep "analyse:	false	c" | wc -l`

total_heur=`grep HEURISTIC ${file} | grep -v "analyse:	false	NA" | wc -l`
tp_heur=`grep HEURISTIC ${file} | grep "analyse:	true	c" | wc -l`
fp_heur=`grep HEURISTIC ${file} | grep "analyse:	false	c" | wc -l`

p_e=`echo "${tp_exact} / ${total_exact}" | bc -l`
p_f=`echo "${tp_fuzzy} / ${total_fuzzy}" | bc -l`
p_h=`echo "${tp_heur} / ${total_heur}" | bc -l`
total_matched=$((total_exact + total_fuzzy + total_heur))
sensitivity=`echo "${total_matched} / ${total_number_of_chemicals_considered}" | bc -l`
echo "
--------------------------------------------------------
#chemicals: ${total_number_of_chemicals}
#no refences: ${no_reference_id}
#considered: ${total_number_of_chemicals_considered}
TYPE:	Matched	True	False	Precision
________________________________________________________
EXACT	${total_exact}	${tp_exact}	${fp_exact}	${p_e}
________________________________________________________
FUZZY	${total_fuzzy}	${tp_fuzzy}	${fp_fuzzy}	${p_f}
________________________________________________________
HEURI	${total_heur}	${tp_heur}	${fp_heur}	${p_h}
________________________________________________________
"
echo "sensitivity: ${sensitivity}"
