### specific to the name matching
# $1 INPUT foreground
# $2 INPUT background
# $3 INPUT fuzzy
# $4 INPUT heuristic
# $5 OUTPUT output-foreground
# $6 OUTPUT output-background
### specific to the enrichment
# $7 INPUT database-list
# $8 INPUT significance level
# $9 INPUT correction-test
# $10 INPUT foreground-enrichment
# $11 OUTPUT output-enr (nur 1 !!)
# $12 OUTPUT output-ann (nur 1 !!)
# $13 OUTPUT output_network_link
# $14 html output form visualization modules (table with links)
# $15 synonym output

echo "TEST: $17"
echo "input foreground --> $1"
echo "input background -->  $2"
echo "fuzzy  --> $3"
echo "heuristic --> $4"
echo "output foreground --> $5"
echo "output background --> $6"
echo "databses --> $7"
echo "alpha --> $8"
echo "correction test --> $9"
echo "input enrichment --> ${10}"
echo "output enrichment enr --> ${11}"
echo "output enrichment ann --> ${12}"
echo "output network --> ${13}"
echo "output table  --> ${14}"
echo "synonym output --> ${15}"
echo "synonym file --> ${16}"
echo "input type --> ${17}"
echo "output scores --> ${18}"
echo "levenshtein distance --> ${19}"
echo `which python`

dbs=$7
dbs=(${dbs//,/ })
backg=$6
DET_DIRECTORY="/home/det/galaxy-dist-v2/tools/det"
NAME_MATCHING_SCRIPT=${DET_DIRECTORY}/src/name_matching.py
ENRICHMENT_SCRIPT=${DET_DIRECTORY}/src/enrichment_calculation.py
NETWORK_RENDERER_SCRIPT=${DET_DIRECTORY}/src/network_generator.py
LINKS_TABLE_GENERATOR=${DET_DIRECTORY}/src/result_annotator.py
APPEND_SCRIPT=${DET_DIRECTORY}/src/append-to-file.py
NETWORK_HTML_PAGES_DIRECTORY=/home/det/public_html/static
URL="http://det.embl.de/~det/static"
## MATCH FOREGROUND
echo "#################################################"
echo "##### NAME MATCHING COMMAND: FOREGROUND #########"
echo "#################################################"
echo "python ${NAME_MATCHING_SCRIPT} -n $1 -o $5 -a $3 -e $4 -t ${17}"
python ${NAME_MATCHING_SCRIPT} -n $1 -o $5 -d ${18} -a $3 -e $4 -t ${17} -l ${19}
## MATCH BACKGROUND IF ONE SELECTED
if [[ $2 == "None" ]]
    then
        echo "No background provided"
        backg='ALL'
    else
        echo "#################################################"
        echo "##### NAME MATCHING COMMAND: BACKGROUND #########"
        echo "#################################################"
        echo "python ${NAME_MATCHING_SCRIPT} -n $2 -o $6 -a $3 -e $4 -t ${17}"
        python ${NAME_MATCHING_SCRIPT} -n $2 -o $6 -a $3 -e $4 -t ${17} -d ${18} -l ${19}
        backg=$6
fi
## ENRICHMENT
echo -e 'property\tdatabase\tcorrected p value\tp value\todds ratio\tn_r' > ${11}
col_ent=`mktemp`
tmp_ann=`mktemp`
for i in "${!dbs[@]}"
    do
        db=${dbs[i]}
    ### create tmp files (one for the enrichment and one for the annotation)
        file_enr=`mktemp`
        file_ann=`mktemp`
    ### compute enrichment and write results of database to respective tmp files
    echo "####################################"
    echo "##### COMPUTING ENRICHMENT #########"
    echo "####################################"
    echo "python ${ENRICHMENT_SCRIPT} -f $5 -b $backg -d $db -c $9 -a $8 -o $file_enr -p $file_ann"
    python ${ENRICHMENT_SCRIPT} -f $5 -b $backg -d $db -c $9 -a $8 -o $file_enr -p $file_ann
    ### append results to final output files to be returned to galaxy
        tail -n +2 "$file_enr" >> ${11}
        cut -f3 "$file_ann" > $col_ent
echo "test_c"
echo "${12}"
echo $col_ent
echo $tmp_ann
        paste ${12} $col_ent > $tmp_ann
echo "test_d"
        mv $tmp_ann ${12}
    done
cut -f1,2 "$file_ann" > $col_ent
echo "test_a"
echo "${12}"
echo $col_ent
echo $tmp_ann
paste $col_ent ${12}  > $tmp_ann
echo "test_b"
mv $tmp_ann ${12}
### generate the tables with external links
echo "####################################"
echo "##### GENERATING HTML TABLE ########"
echo "####################################"
echo "python ${LINKS_TABLE_GENERATOR} -i ${11} -o ${14}"
python ${LINKS_TABLE_GENERATOR} -i ${11} -o ${14}
### produce the network
file_network_html_page_tmp=`mktemp`
network_html_page=${NETWORK_HTML_PAGES_DIRECTORY}/$(basename "$file_network_html_page_tmp").html
network_html_page_adress=${URL}/$(basename "$file_network_html_page_tmp").html
echo "####################################"
echo "##### GENERATING HTML NETWORK ######"
echo "####################################"
echo "python ${NETWORK_RENDERER_SCRIPT} -a ${12} -e ${11} -o ${network_html_page}"
python ${NETWORK_RENDERER_SCRIPT} -a ${12} -e ${11} -o ${network_html_page}



#echo "<!doctype html><head><script >function load(){window.location.href="$network_html_page_adress";}</script></head><body onload=\"load()\"><a href=\""$network_html_page_adress"\">Click here</a></body></html>" > ${13}
echo "<!doctype html><head></head><body>Please click <a href=\""$network_html_page_adress"\">here </a>to open the dynamic network in this page</body></html>" > ${13}
### synonyms

if [[ ${15} == "false" ]];then
    echo "####################################"
    echo "##### LOOKING FOR SYNONYMS     ######"
    echo "####################################"
    echo "Synonyms: ${15}"
    echo "Synonym output file: ${16}"
    echo "Synonym output file: ${3}"
    python ${NAME_MATCHING_SCRIPT} -n $1 -o ${16} -s true -a $3 -e $4 -t ${17} -d ${18}
fi
