from __future__ import division
from _pyio import open
from solr_controller import SolrController
import sys
reload(sys)
sys.setdefaultencoding("utf-8")
import argparse
import os


def standardizeCid(cid):
    cid = str(cid)
    while(len(cid) < 9):
        cid = "0" + cid
    return "cid" + cid

### @ reference_matching:
###    This is the standard to which the name matching results should be compared to. It is a string representing a two tab delimited column file
###    whose first column is the chemical name and the second column is the cid
###
### @ results_matching :
###    This is the results of the name matching tool using the chemical present in the 'first column' of reference_matching. It is a string representing
###    a two tab delimited column file whose first column is the chemical name and the second column is the cid
def benchmarkPrecision(reference_matching, results_matching):
    true_positive_e = 0
    false_positive_e = 0
    true_positive_f = 0
    false_positive_f = 0
    true_positive_a = 0
    false_positive_a = 0
    true_positive = 0
    false_positive = 0
    reference_matching = reference_matching.split("\n")
    results_matching = results_matching.split("\n")
    reference = {}
    total = 0
    total_considered = 0
    matched = 0
    uncorrectly_matched_ids = "chemical\treference\tfetched\n"
	# build a hash out of the reference matching to check the cid matches more quickly
    for match in reference_matching:
		chemical = match.split("\t")[0]
		cid = match.split("\t")[1]
		#reference[chemical] = cid
		if cid != "NA":
		    reference[chemical] = cid
		    total_considered += 1
	# now check that the cid matched match the reference one\
    cc = 0
    for match in results_matching:
        total = total + 1
        chemical = match.split("\t")[0]
        cid_matched = match.split("\t")[1]
        match_type = match.split("\t")[2]
        cid_reference = "NA"
        if chemical in reference:
            cid_reference = reference[chemical]
        if not(cid_matched.startswith("cid")) and cid_matched != "NA":
            cid_matched = standardizeCid(cid_matched)
        if not(cid_reference.startswith("cid")) and cid_reference != "NA":
            cid_reference = standardizeCid(cid_reference)
        if cid_reference != "NA" and cid_reference == cid_matched:
            true_positive = true_positive + 1
            if match_type == "EXACT_MATCH":
                true_positive_e = true_positive_e + 1
            if match_type == "FUZZY_MATCH":
                true_positive_f = true_positive_f + 1
            if match_type == "HEURISTIC_MATCH":
                cc += 1
                true_positive_a = true_positive_a + 1            
            matched = matched + 1
        elif cid_reference != "NA" and (cid_matched != cid_reference and cid_matched != 'NA'):
            matched = matched + 1
            uncorrectly_matched_ids = uncorrectly_matched_ids + chemical + "\t" + cid_reference + "\t" + cid_matched + "\n"
            false_positive = false_positive + 1
            if match_type == "EXACT_MATCH":
                false_positive_e = false_positive_e + 1
            if match_type == "FUZZY_MATCH":
                false_positive_f = false_positive_f + 1
            if match_type == "heuristic_MATCH":
                cc += 1
                false_positive_a = false_positive_a + 1
        bool_correct = 'false'
        if cid_reference == cid_matched:
            bool_correct = 'true'
            print "analyse:\t" + bool_correct + "\t" + cid_reference + "\t" + match
        else:
            print "analyse:\t" + bool_correct + "\t" + cid_reference + "\t" + match
    precision = str((true_positive / (true_positive + false_positive)) * 100) if (true_positive + false_positive) != 0 else 'Could not be computed' 
    precision_e = str((true_positive_e / (true_positive_e + false_positive_e)) * 100) if (true_positive_e + false_positive_e) != 0 else 'Could not be computed' 
    precision_f = str((true_positive_f / (true_positive_f + false_positive_f)) * 100) if (true_positive_f + false_positive_f) != 0 else 'Could not be computed'
    precision_a = str((true_positive_a / (true_positive_a + false_positive_a)) * 100) if (true_positive_a + false_positive_a) != 0 else 'Could not be computed'

    print "\n####"
    print "#### RESULTS BENCHMARKING"
    print "####\n"
    print "Correctly matched (true positive): " + str(true_positive)
    print "Wrongly matched or not found (false positive) : " + str(false_positive)
    print "Precision : " + precision + " %(tp: " + str(true_positive) + " ; fp: " + str(false_positive) + ")" 
    print "----Precision (exact) : " + precision_e + " % (" + str(true_positive_e) + " ; " + str(false_positive_e) + ")" 
    print "----Precision (fuzzy) : " + precision_f + " % (" + str(true_positive_f) + " ; " + str(false_positive_f) + ")" 
    print "----Precision (approximate): " + precision_a + " %(" + str(true_positive_a) + " ; " + str(false_positive_a) + ")" 
    print "Sensitivity : " + str((matched / total_considered ) * 100) + "(matched  " + str(matched) + " , total considered = " + str(total_considered) + ", total = " + str(total) + ")"
    print "\n####"
    print len(reference_matching)
    print len(results_matching)
    print "#### END RESULTS BENCHMARKING"
    print "####\n"
    print "Results have been written here: " + os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)) + '/test/benchmark-output-tmp.tsv'
    var = raw_input("Would you like to have a look at the mismatches ? (y/n) ")
    if var == "y":
        var = raw_input("Enter a file name (watch out, this will overwrite any existing file !) : ")
        f = open(var,"w")
        f.write(uncorrectly_matched_ids)
        f.close()
        print "the mismatches have been written to " + var

### How it works :
###    1) Select a benchmarking set (it needs to be a two tab delimited column file
###    2) Extract the chemical of the first column to perform the name matching
###    3) Reparse the results of the name matching to only have the format required by the function benchmarkSensitivity
###    4) Compute the sensitivity
def benchmarkController(dataset_to_benchmark,universe):
    print "###"
    print "### Performing benchmarking ...."
    print "###"
    ## Get the reference matches
    print("dataset to benchmark is " + dataset_to_benchmark)
    reference_matching = ""
    with open(dataset_to_benchmark, 'r') as content_file:
        reference_matching_content = content_file.read()
    reference_matching_content = reference_matching_content.rstrip()  
    ## Extract the chemical to prepare the query and create a file to start the name matching
    reference_matching = reference_matching_content.split("\n")
    chemical_for_query = []
    namefile = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)) + '/test/query.tmp.tsv'
    outputfile = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)) + '/test/benchmark-output-tmp.tsv'
    f = open(namefile,'w')
    for match in reference_matching:
        f.write(match.split('\t')[0] + '\n')
    f.close()
    ## perform name matching and get the results under the 2 tab delimited string 
    sc = SolrController("names","output-to-delete",4, search_space=universe)
    print("second results will be written here : " + outputfile)
    sc.matchNames(namefile, outputfile, True, True, True)
    with open(outputfile, 'r') as content_file:
        results_name_matching = content_file.read()
    ## reparsse the result of the name matching to only get two column
    results_name_matching = results_name_matching.split("\n")
    results_name_matching_correctly_parsed = ""
    results_name_matching = results_name_matching[:-1]
    for match in results_name_matching:
        match = match.split("\t")
        results_name_matching_correctly_parsed = results_name_matching_correctly_parsed + match[2] + "\t" + match[0] + "\t" + match[4] + "\n"
    results_name_matching_correctly_parsed = results_name_matching_correctly_parsed.rstrip()
    ## compute sensitivity
    benchmarkPrecision(reference_matching_content, results_name_matching_correctly_parsed)


### called for if indirect option is TRUE. This function assumes that the second column of the file is a reference id
### that can be exactly matched ot retrieve a cid. The resulting CID will be considered as the TRUE cid corresponding to
### the chemical associated with the reference id.
def prepareBenchMarkFile(query_file, matching_file,universe):
    print "###"
    print "### Prepapring file for benchmarking ...."
    print "### Matching reference ids to get TRUE cids"
    print "###"
    ### perform name matching with the reference id
    outputfile = 'results-name-matching.tsv'
    sc = SolrController("names","out-to-delete",4, search_space=universe)
    print("Results of the reference ids matching will be written to : " + outputfile)
    sc.matchNames(query_file, outputfile, False, True, False)
    with open('results-name-matching.tsv', 'r') as content_file:
        results_name_matching = content_file.read()
    results_name_matching = results_name_matching.rstrip()
    results_name_matching = results_name_matching.split('\n')[1:]
    second_reference_id_to_cid_dic = {}
    for line in results_name_matching:
        elems = line.split('\t')
        second_reference_id_number = elems[2]
        cid = elems[0]
        second_reference_id_to_cid_dic[second_reference_id_number] = cid
    print("Matching file is : " + matching_file)
    with open(matching_file, 'r') as content_file:
        results_name_matching = content_file.read()
    results_name_matching = results_name_matching.rstrip()
    results_name_matching = results_name_matching.split('\n')
    second_reference_id_to_chemical_dic = {}
    for line in results_name_matching:
        elems = line.split('\t')
        chemical_name = elems[0]
        second_id = elems[1]        
        second_reference_id_to_chemical_dic[second_id] = chemical_name

    second_id_that_were_matched_exactly = second_reference_id_to_cid_dic.keys()
    chemical_name_to_cid_reference = ""
    for matched_second_id in second_id_that_were_matched_exactly:
        chemical_name = second_reference_id_to_chemical_dic[matched_second_id]
        cid = second_reference_id_to_cid_dic[matched_second_id]
        chemical_name_to_cid_reference = chemical_name_to_cid_reference + chemical_name + '\t' + cid + '\n'
    return chemical_name_to_cid_reference.rstrip()
        
         

### Benchmarking precision / sensitivity can be done in 2 ways : directly or undirectly
###        Directly: the file provided needs to have two columns. The first one is the chemical name 
###                  and the second one is a CID (either STITCH or PUBCHEM)
###        Indirectly: the file provided contains two columns. The first one is the chemical name 
###                    and the second one is an ID that in theory can be matched exactly (like a CAS number for instance).
###                    In that case, a first name matching (exact) is performed with that second id to in order to fetch
###                    the true CIDs. If no CID could be found, then we do not consider it for the benchmarking
if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Benchmarking options', version='0.1')
    parser.add_argument('-f', '--filename', type=str, help='benchmarking file')
    parser.add_argument('-d', '--direct', type=str, help='benchmarking file')
    parser.add_argument('-u', '--universe', type=str, help='universe (S or P')

    args = parser.parse_args()
    benchmark_file = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)) + '/tmp/benchmarking-set.tsv'
    query_file = os.path.abspath(os.path.join(os.path.dirname(os.path.realpath(__file__)), os.pardir)) + '/tmp/reference-ids-file.txt'
    matching_file = args.filename
    universe = ""
    if args.universe.upper() == "S":
        universe = "stitch20141111"
    if args.universe.upper() == "P":
        universe = "pubchem-unfiltered20141110"
    if args.direct.upper() == 'F':
        reference_ids = []
        with open(args.filename) as content:
            content = content.read()
        content = content.rstrip()
        content = content.split('\n')
        ## get the reference ids (assumed to be in the second column)
        for line in content:
            elems = line.split('\t')
            reference_ids.append(elems[1])
        print("debug : benchmarking sensitivity INDIRECT, " + str(len(reference_ids)) + " reference IDs")
        ## write the reference ids to a file
        f = open(query_file,'w')
        reference_ids = '\n'.join(reference_ids)
        f.write(reference_ids)
        f.close()
        
        reference_string = prepareBenchMarkFile(query_file,matching_file,universe)
        
        f = open(benchmark_file,'w')
        f.write(reference_string)
        f.close() 
        benchmarkController(benchmark_file,universe)
    else:
        benchmarkController(args.filename,universe)
        
    
########### EXAMPLES
#SELLECK_DS=/g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/test/benchmarking/sensitivity-precision/benchmark-selleck.tsv
#SELLECK_DS_S=/g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/test/benchmarking/sensitivity-precision/benchmark-selleck.tsv
#CMAP_DS=/g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/test/benchmarking/sensitivity-precision/benchmark-cmap.tsv
#PRESTWICK_DS=/g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/test/benchmarking/sensitivity-precision/benchmark-prestwick.tsv
#TTD_DS=/g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/test/benchmarking/sensitivity-precision/benchmark-ttd.tsv
######################## BENCHMARKING CMAP
#python benchmarking_sensitivity.py -f $CMAP_DS -d t -u s
######################## BENCHMARKING SELLECK
#python benchmarking_sensitivity.py -f $SELLECK_DS_S -d f -u s
######################## BENCHMARKING PRESTWICK
#python benchmarking_sensitivity.py -f $PRESTWICK_DS -d f -u s 
######################## BENCHMARKING TTD
#python benchmarking_sensitivity.py -f $TTD_DS -d t -u p
#################
################# RESULTS
#################
                                            ########### SENSITIVITY    PRECISION
                                            ## CMAP       94%            96%
                                            ## SELLECK    93%            97%
                                            ## PRESTWICK  99%            98%
                                            ## TTD        99%            90%
