DET_DIRECTORY="/home/det/galaxy-dist-v2/tools/det"
NETWORK_RENDERER_SCRIPT=${DET_DIRECTORY}/src/network_generator.py
LINKS_TABLE_GENERATOR=${DET_DIRECTORY}/src/result_annotator.py
NETWORK_HTML_PAGES_DIRECTORY=/home/det/public_html/static
URL="http://det.embl.de/~det/static"
### Generate table with links
python ${LINKS_TABLE_GENERATOR} -i $1 -o $3
### Generate interactive network
file_network_html_page_tmp=`mktemp`
network_html_page=${NETWORK_HTML_PAGES_DIRECTORY}/$(basename "$file_network_html_page_tmp").html
network_html_page_adress=${URL}/$(basename "$file_network_html_page_tmp").html
echo ${network_html_page}
python ${NETWORK_RENDERER_SCRIPT} -a ${2} -e ${1} -o ${network_html_page}
echo $network_html_page_adress
echo "<!doctype html><head></head><body>Please click <a href=\""$network_html_page_adress"\">here </a>to open the dynamic network in this page</body></html>" > ${4}


