#!/bin/bash

## activate environment
source /home/deghou/virtualenvs/python-2.7/bin/activate
## data sources
SELLECK_DS=/g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/test/benchmarking/sensitivity-precision/benchmark-selleck.tsv
SELLECK_DS_S=/g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/test/benchmarking/sensitivity-precision/benchmark-selleck.tsv
CMAP_DS=/g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/test/benchmarking/sensitivity-precision/benchmark-cmap.tsv
PRESTWICK_DS=/g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/test/benchmarking/sensitivity-precision/benchmark-prestwick.tsv
TTD_DS=/g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/test/benchmarking/sensitivity-precision/benchmark-ttd.tsv

## BENCHMARKING CMAP
python benchmarking_sensitivity.py -f $CMAP_DS -d t -u s > results-benchnmarking-sensitivity-cmap.txt
## BENCHMARKING SELLECK
#python benchmarking_sensitivity.py -f $SELLECK_DS_S -d f -u s > results-benchnmarking-sensitivity-selleck.txt
## BENCHMARKING PRESTWICK
#python benchmarking_sensitivity.py -f $PRESTWICK_DS -d f -u s > results-benchnmarking-sensitivity-prestwick.txt
## BENCHMARKING TTD
#python benchmarking_sensitivity.py -f $TTD_DS -d t -u p > results-benchnmarking-sensitivity-ttd.txt

