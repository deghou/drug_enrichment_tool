from __future__ import division
import networkx as nx
import matplotlib.pyplot as plt
import os
from networkx import graphviz_layout
import operator
import numpy as np
from unittest.util import sorted_list_difference
import argparse


class GraphViz:
    
    def __init__(self):
        self.chemical_term_annotations = ""
        self.layout = {}
        self.layout_name = "default"
        self.networkxgraph = {}
        self.dictInteraction = {}
        self.chem_to_term_dic = {}
        self.term_to_chem_dic = {}
    
    ###
    ### SETTERS
    ###
    def setChemicalTermAnnotation(self,chemical_term_annotations):
        self.chemical_term_annotations = chemical_term_annotations

    def setLayout(self,layout):
        self.layout = layout   
    
    def setNetworkXGraph(self,networkxgraph):
        self.networkxgraph = networkxgraph 
    ###
    ### GETTERS
    ### 
    def getChemicalTermAnnoation(self):
        return self.chemical_term_annotations
    
    def getLayout(self):
        return self.layout

    def getNetworkXGraph(self,networkxgraph):
        return set.networkxgraph 

    def extractNodeConnectivity(self):
        #prepare graph
        chemicals = dict()
        terms = dict()
        nodeConnectivity = {}
        dictInteraction = {}
        for interaction in self.chemical_term_annotations:
            interaction = interaction.rstrip()
            chemical = interaction.split("\t")[0]
            term = interaction.split("\t")[1]
            if chemical in chemicals:
                dictInteraction[chemical].append(term)
                chemicals[chemical] += 1
            else:
                dictInteraction[chemical] = [term]
                chemicals[chemical] = 1
            if term in terms:
                terms[term] += 1
            else:
                terms[term] = 1
        nodeConnectivity['chemical_connectivity'] = chemicals
        nodeConnectivity['term_connectivity'] = terms
        nodeConnectivity
        ### Not very beautiful .. needs to be refactored
        self.dictInteraction = dictInteraction
        return nodeConnectivity
      
    def sortNodesPerConnectivityDegree(self):
        nodeConnectivity = self.extractNodeConnectivity()
        sortedNodes = {}
        #sort
        chemicals = nodeConnectivity["chemical_connectivity"] # dict
        terms = nodeConnectivity["term_connectivity"] # dict
        chemicals = sorted(chemicals.iteritems(), key=operator.itemgetter(1)) # list of tuples
        terms = sorted(terms.iteritems(), key=operator.itemgetter(1)) # list of tuples
        #add nodes
        chemicals_ordered = [(i[0]) for i in chemicals]
        terms_ordered = [(i[0]) for i in terms]
        sortedNodes["chemicals_sorted"] = chemicals_ordered
        sortedNodes["terms_sorted"] = terms_ordered
        return(sortedNodes) 

    def createGraph(self):
        #sort nodes per connectivity degree
        sortedNodes = self.sortNodesPerConnectivityDegree()
        chemicals_ordered = sortedNodes["chemicals_sorted"]
        terms_ordered = sortedNodes["terms_sorted"]
        #create graph
        G=nx.cycle_graph(0)
        G.add_nodes_from(chemicals_ordered)
        G.add_nodes_from(terms_ordered)
        #add edges
        for interaction in self.chemical_term_annotations:
            interaction = interaction.rstrip()
            chemical = interaction.split("\t")[0]
            term = interaction.split("\t")[1]
            G.add_edge(chemical,term)
        return G
     
    def countCrossingEdges(self):
        edges_coordinate = []
        for edge in self.networkxgraph.edges():
            node_1,node_2 = edge
            node_drug = ''
            node_enri = ''
            if node_1.startswith('enrichment_term'):
                node_enri = node_1
                node_drug = node_2
            else:
                node_enri = node_2
                node_drug = node_1
            coord_x_drug,coord_y_drug = self.layout[node_drug]
            coord_x_enri,coord_y_enri = self.layout[node_enri]
            edges_coordinate.append([(coord_x_drug,coord_y_drug),(coord_x_enri,coord_y_enri)])
        crossing = 0
        edge_number = len(edges_coordinate)
        m = 0
        while m <= edge_number - 1:
            edge_m = edges_coordinate[m]
            n = m + 1
            while n <= edge_number - 1:
                edge_n = edges_coordinate[n]
                n += 1
                if self.areEdgesCrossing(edge_m,edge_n):
                    crossing += 1
            m += 1
        return crossing
        
    def areEdgesCrossing(self,edge_m,edge_n):
        x11,y11 = edge_m[0]
        x12,y12 = edge_m[1]
        x21,y21 = edge_n[0]
        x22,y22 = edge_n[1]
        if ((y11 > y21) and (y12 < y22)) or ((y11 < y21) and (y12 > y22)): 
            return True
        else:
            return False
                      
    def findLayout(self):
        #create layout
        layout_drugs = {}
        layout_terms = {}
        x = 2
        y = 0
        y_max = 100
        previous_number_of_crossings = -1        
        for term in self.term_to_chem_dic.viewkeys():
            y = y + y_max/len(self.term_to_chem_dic)
            layout_terms[term] = (x,y)
        iteration = 1
        while True:
            print "Iteration " + str(iteration)
            if iteration%2 == 0:
                dictInteraction = self.term_to_chem_dic
                nodes = self.term_to_chem_dic.viewkeys()
                layout_to_change = layout_terms
                reference_layout = layout_drugs
                new_x = 2
            else:
                dictInteraction = self.chem_to_term_dic
                nodes = self.chem_to_term_dic.viewkeys()
                layout_to_change = layout_drugs
                reference_layout = layout_terms
                new_x = 0
            x = 0 ; y = 0
            for node in nodes:
                partners = dictInteraction[node]
                y_coords_partners = []
                for part in partners:
                    x,y = reference_layout[part]
                    y_coords_partners.append(y)
                y_median = np.median(y_coords_partners)
                layout_to_change[node] = (new_x,y_median)
            sorted_layout_to_change = sorted(layout_to_change.iteritems(), key=operator.itemgetter(1))
            sorted_layout_to_change.reverse()
            nl = {}
            d_index = 0
            ### stretch chemical's relative position so that they take all the available space
            for node in sorted_layout_to_change:
                (x,y) = node[1]
                elem = node[0]
                y_stretched = (y + (y_max - y)) - ((y_max/len(sorted_layout_to_change)) * d_index)
                d_index += 1
                layout_to_change[node] = (x,y_stretched)
                nl[elem] = (x,y_stretched)
                if iteration%2 == 0:
                    layout_terms = nl
                else:
                    layout_drugs = nl
            layout = dict(reference_layout.items() + nl.items())
            self.layout = layout
            if self.countCrossingEdges() == previous_number_of_crossings:
                break
            previous_number_of_crossings = self.countCrossingEdges()
            print "Number of crossing edges " + str(self.countCrossingEdges())
            iteration += 1
            if iteration == 200:
                break
        return layout
                  
    def buildInteractionDics(self):
        chem_to_term_dic = {}
        term_to_chem_dic = {} 
        #print "Chemical term annotations : "
        print "\n"
        #print self.chemical_term_annotations
        for interaction in self.chemical_term_annotations:
            chem = interaction.split("\t")[0]
            term = interaction.split("\t")[1].rstrip()
            if chem in chem_to_term_dic:
                chem_to_term_dic[chem].append(term)
            else:
                chem_to_term_dic[chem] = [term]
            if term in term_to_chem_dic:
                term_to_chem_dic[term].append(chem)
            else:
                term_to_chem_dic[term] = [chem]
        self.chem_to_term_dic = chem_to_term_dic
        self.term_to_chem_dic = term_to_chem_dic

    def drawGraph(self,network_output_file):
        #plot
        #draw nodes
        node_size = 150
        plt.figure(figsize=(12,12)) 
        label_font_size = 5
        for node in self.networkxgraph.nodes():            
            x,y=self.layout[node]
            if node.startswith("drug_"):
                nx.draw_networkx_nodes(self.networkxgraph,self.layout,node_size=node_size,nodelist=[node],node_shape = 'o',node_color = "white")                
                plt.text(x - 0.15,y,s=node.split("drug_")[1], horizontalalignment='center',fontsize = label_font_size)
            elif node.startswith('enrichment_term_'):
                nx.draw_networkx_nodes(self.networkxgraph,self.layout,node_size=node_size,nodelist=[node],node_shape = 'd', node_color = 'white')
                plt.text(x + 0.15,y,s=node.split("enrichment_term_")[1], horizontalalignment='center',fontsize = label_font_size)
        #draw edges
        color_edges = ["#000000","#0A9208","#C314D7","#0062E2","#FF0000","#82F74C","#FFBB0D","#5100FF"]
        color_i = 0
        drug_layout = {k:v for k,v in self.layout.iteritems() if k.startswith("drug_")}
        drug_layout_sorted = sorted(drug_layout.iteritems(), key=operator.itemgetter(1))
        drugs_sorted_by_y_coords = [str(i[0]) for i in drug_layout_sorted]
#        for node in self.chem_to_term_dic.viewkeys():
        for node in drugs_sorted_by_y_coords:
            edges = []            
            for partner in self.chem_to_term_dic[node]:
                edges.append((node,partner))
            nx.draw_networkx_edges(self.networkxgraph,self.layout,alpha = 0.5, edgelist = edges, edge_color = color_edges[color_i],width = 2)
            color_i += 1
            if color_i == len(color_edges):
                color_i = 0
        plt.axis('off')
        plt.savefig(network_output_file)
        #plt.show() 

    def updateNetwork(self,test):
        prefix = test.split("_")[0]
        for node in test.split("_")[1:]:
            self.networkxgraph = nx.relabel_nodes(self.networkxgraph,{"drug_" + node : test})
        
    def xCoordinatesToConcave(self):
        drug_layout = {k:v for k,v in self.layout.iteritems() if k.startswith("drug_")}
        drug_layout_sorted = sorted(drug_layout.iteritems(), key=operator.itemgetter(1))
        drugs_sorted_by_y_coords = [str(i[0]) for i in drug_layout_sorted]

        term_layout = {k:v for k,v in self.layout.iteritems() if k.startswith("enrichment_term_")}        
        term_layout_sorted = sorted(term_layout.iteritems(), key=operator.itemgetter(1))
        term_sorted_by_y_coords = [str(i[0]) for i in term_layout_sorted]
        distance = 20
        l = len(drug_layout_sorted)
        i = -1
        j = -1
        for d in drug_layout_sorted:
            drug,coordinates = d
            x,y = coordinates
            if j >= (l/2):
                i -= 1
                j += 1
            else:
                i += 1
                j += 1
            new_x = x - (i/distance)
            self.layout[drug] = (new_x,y)
        l = len(term_layout_sorted)
        i = -1
        j = -1
        for d in term_layout_sorted:
            term,coordinates = d
            x,y = coordinates
            if j >= (l/2):
                i -= 1
                j += 1
            else:
                i += 1
                j += 1
            new_x = x + (i/distance)
            self.layout[term] = (new_x,y)
        
            
            
            

                
    def mergeNodes(self):
        drugs = list(self.chem_to_term_dic.viewkeys())
        terms = list(self.term_to_chem_dic.viewkeys())
        drugs_dic_copy = self.chem_to_term_dic.copy()
        terms_dic_copy = self.term_to_chem_dic.copy()
        merged = {}
        m = 0
        ### first merge nodes
        while m < len(drugs) - 1:
            drug = drugs[m]
            print drug  
            n = m + 1
            drug_neighboor = self.chem_to_term_dic[drug]
            new_merged_name = "drug_" + drug.split('drug_')[1]
            while n < len(drugs):
                next_drug = drugs[n]
                next_drug_neighboor = self.chem_to_term_dic[next_drug] 
                ### do the drugs have the same neighboor ??
                if set(drug_neighboor) == set(next_drug_neighboor):
                    drugs_dic_copy.pop(next_drug,None)
                    n -= 1
                    drugs.remove(next_drug)
                    if drug in drugs_dic_copy:
                        drugs_dic_copy.pop(drug,None)
                    new_merged_name = new_merged_name  + "_" + next_drug.split("drug_")[1]
                n += 1
            ### if drugs have been merged then replace the nodes and labels in both dictionary
            if len(new_merged_name.split("_")) > 2:
                self.updateNetwork(new_merged_name)
                drugs_dic_copy[new_merged_name] = drug_neighboor
                for nb in drug_neighboor:
                    drugs_of_term = terms_dic_copy[nb]
                    i = 0
                    while i < len(drugs_of_term):
                        if drugs_of_term[i].split("_")[1] in new_merged_name:
                            drugs_of_term[i] = new_merged_name
                        i += 1
                    terms_dic_copy[nb] = list(set(drugs_of_term))
            m += 1
        self.chem_to_term_dic = drugs_dic_copy
        self.term_to_chem_dic = terms_dic_copy        
            


def __main__():
    parser = argparse.ArgumentParser(description='Draw a network', version='0.1')
    parser.add_argument('-n', '--network', type=str, help='file name of the drug foreground')
    parser.add_argument('-o', '--output', type=str, default='ALL', help='file name of the drug background')
    parser.add_argument('-ho', '--htmloutput', type=str, help='name of the output file to which html will be written')
    args = parser.parse_args()
    # read network
    assert(args.network is not None)
    assert(args.output is not None)
    with open(args.network, 'r') as content_file:
        network = content_file.read()
    network = network.rstrip()
    chemical_term_annotations = network.split('\n')
    if len(chemical_term_annotations) > 1:
        graphViz = GraphViz()
        graphViz.setChemicalTermAnnotation(chemical_term_annotations)
        graphViz.buildInteractionDics()
        # create/set graph
        G = graphViz.createGraph()
        graphViz.setNetworkXGraph(G)
        # create/set layout
        graphViz.layout_name = "default"
    #    print graphViz.countCrossingEdges()
        graphViz.mergeNodes()
        layout = graphViz.findLayout()
        graphViz.setLayout(layout)
    #    graphViz.xCoordinatesToConcave()
        # draw graph
        graphViz.drawGraph(args.output)
        ### Display the svg in the 
        h = open(args.htmloutput, 'w')
        h.write("<img src=" + args.output + " alt=\"network\">")
        h.close()
if __name__ == '__main__': __main__()
