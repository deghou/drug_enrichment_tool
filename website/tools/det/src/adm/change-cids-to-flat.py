import argparse

## AUFRUFEN
#curl 'http://localhost:8983/solr/admin/cores?action=CREATE&name=stitch20141111&config=solrconfig.xml&schema=schema.xml&dataDir=data'
#curl 'http://localhost:8984/solr/admin/cores?action=CREATE&name=pubchem&config=solrconfig.xml&schema=schema.xml&dataDir=data'
## AUFLADEN
#tacurl 'http://localhost:8983/solr/stitch20141111/update/csv?commit=true&separator=%09&escape=\&stream.file=/g/bork8/deghou/projects/enrichment_tool/file_for_solr_index/file_for_solr_index/cids-that-needs-to-be-added-based-on-flat.tsv'
#curl 'http://localhost:8984/solr/pubchem/update/csv?commit=true&separator=%09&escape=\&stream.file=/g/bork8/deghou/projects/enrichment_tool/file_for_solr_index/file_for_solr_index/chemical_universe_pubchem_filtered_cleaned_flat_with_cid_synonyms.tsv'

parser = argparse.ArgumentParser(description='Matches chemical names to STITCH IDs', version='0.1')
parser.add_argument('-f', '--file', type=str, help='file')
parser.add_argument('-m', '--mapping', type=str, help='mapping file')

args = parser.parse_args()

file = args.file
mapping_file = args.mapping

d = {}
with open(mapping_file) as f:
    for line in f:
       (flat, nonflat) = line.split()
       d[nonflat] = flat


changed = 0
ff = open(file + "_flat","w")
id = 1
with open(file) as f:
    next(f)
    for line in f:
        l = line.split("\t")
        cid = l[0]  
        chem = l[1]
        if cid in d:
            cid = d[cid]
            changed +=1
        ff.write(str(id) + "\t" + cid + "\t" + chem + "\n")
        id += 1
    print changed
ff.close()