import os



DATABASE_DIR = "/g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/databases/"

d = {}
with open("/g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/stitch-stereo-flat-v4.txt") as f:
    for line in f:
       (flat, nonflat) = line.split()
       d[nonflat] = flat


databases = os.listdir("/g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/databases")
for d in databases:
    if d.endswith("txt"):
        print d + " ..."
        name,extension = os.path.splitext(d)
        ff = open(DATABASE_DIR + name + "_flat" + extension,"w")
        with open(DATABASE_DIR + d) as f:
            for line in f:
                l = line.split("\t")
                cid = l[0]
                term = l[1].rstrip()
                if cid in d:
                    print "New id for " + d + " : " + cid + " ---> " + d[cid]
                    cid = d[cid]
                if cid.startswith("cid1"):
                    cid = "cid0" + cid[4:]
                ff.write(cid + "\t" + term + '\n')               
        ff.close()
    
    
