import argparse

parser = argparse.ArgumentParser(description='Matches chemical names to STITCH IDs', version='0.1')
parser.add_argument('-a', '--aliases', type=str, help='file')
parser.add_argument('-c', '--chemicals', type=str, help='file')
parser.add_argument('-u', '--universe', type=str, help='mapping file')

args = parser.parse_args()
aliases_file = args.aliases
chemicals_file = args.chemicals
universe_file = args.universe

d = {}
i = 0
j = 1
cid = ''
ff = open(universe_file,"w")
id = 1
ff.write("id\ttitle\tname\n")
with open(aliases_file) as f:
    for line in f:
        ## write to the universe  
        l = line.split('\t')
        ff.write(str(id) + "\t" + str(l[1]) + "\t" + str(l[2]))
        id += 1
        ## load into the dic
        if cid != '' and cid != l[1]:            
            d[l[1]] = True
        cid = l[1]
        ## for verbose purposes
        i += 1
        if i == 100000:
            print str(i * j) + " dic has .. " + str(len(d)) + " entries"
            i = 0
            j += 1

i = 0
j = 1
with open(chemicals_file) as f:
    next(f)
    for line in f:
        l = line.split('\t')
        ## if the id is not in the other file, then add it to the newly created universe
        if not l[1] in d:
            ff.write(str(id) + "\t" + str(l[1]) + "\t" + str(l[2]))
            id += 1
        ## for verbose purposes
        i += 1
        if i == 100000:
            print str(i * j)
            i = 0
            j += 1
ff.close()




