import argparse

parser = argparse.ArgumentParser(description='Matches chemical names to STITCH IDs', version='0.1')
parser.add_argument('-f', '--file', type=str, help='file')
parser.add_argument('-s', '--synonyms', type=str, help='file')

args = parser.parse_args()
file = args.file
synonyms = args.synonyms


f = open(synonyms,"w")
f.write("id\ttitle\tname\n")
id = 1
with open(file, 'r') as content_file:
    name = ""
    id_lucene = ""
    cid = ""
    next(content_file)
    for line in content_file:
        l = line.split("\t")        
        if (l[1] != cid) and (cid != ""):
            f.write(str(id) + "\t" + cid + "\t" + cid + "\n")
            id_lucene = l[0]
            cid = l[1]
            name = l[2]
            id += 1        
            f.write(str(id) + "\t" + l[1] + "\t" + l[2])
        else:
            id_lucene = l[0]
            cid = l[1]
            name = l[2]
            f.write(str(id) + "\t" + cid + "\t" + name)
        id += 1
    f.write(str(id) + "\t" + cid + "\t" + name)
    f.close()        
