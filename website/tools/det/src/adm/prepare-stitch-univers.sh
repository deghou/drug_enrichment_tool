ADM_DIR=pwd
INSTALLATION_DIR=/g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool
#UNIVERSE_DIR=/g/bork8/deghou/projects/enrichment_tool/file_for_solr_index/file_for_solr_index
UNIVERSE_DIR=/g/scb/bork/deghou/stitch
CHEMICALS=http://stitch.embl.de/download/chemicals.v4.0.tsv.gz
ALIASES=http://stitch.embl.de/download/chemical.aliases.v4.0.tsv.gz
MAPPING=/g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/stitch-stereo-flat-v4.txt
CHEMICALS_UNTAR=chemicals.tsv
ALIASES_UNTAR=chemicals_aliases.tsv
STITCH_UNIVERSE=chemicals_universe_stitch

## 1 Download source files and uncompress
cd ${UNIVERSE_DIR}
wget ${CHEMICALS} 
wget ${ALIASES}
gunzip -c `basename ${CHEMICALS}` > ${CHEMICALS_UNTAR} &
gunzip -c `basename ${ALIASES}` > ${ALIASES_UNTAR} &
tr [':upper:'] [':lower:'] < ${CHEMICALS_UNTAR} > x
mv x ${CHEMICALS_UNTAR}
tr [':upper:'] [':lower:'] < ${ALIASES_UNTAR} > x
mv x ${ALIASES_UNTAR}

## 2 conversion to flat ids
cd $ADM_DIR
python change-cids-to-flat.py -f ${UNIVERSE_DIR}/${CHEMICALS_UNTAR} -m ${MAPPING}
python change-cids-to-flat.py -f ${UNIVERSE_DIR}/${ALIASES_UNTAR} -m ${MAPPING}
mv ${UNIVERSE_DIR}/chemicals.tsv_flat ${UNIVERSE_DIR}/${CHEMICALS_UNTAR}
mv ${UNIVERSE_DIR}/chemicals_aliases.tsv_flat ${UNIVERSE_DIR}/${ALIASES_UNTAR}

## 3 creation of stitch universe
python create-stitch-universe.py -a ${UNIVERSE_DIR}/${ALIASES_UNTAR} -c ${UNIVERSE_DIR}/${CHEMICALS_UNTAR} -u ${UNIVERSE_DIR}/${STITCH_UNIVERSE}

## 4 add CID as a synonym of itself
python add-cids-as-synonyms.py -f ${UNIVERSE_DIR}/${STITCH_UNIVERSE} -s ${UNIVERSE_DIR}/${STITCH_UNIVERSE}_synonyms
mv ${UNIVERSE_DIR}/${STITCH_UNIVERSE}_synonyms ${UNIVERSE_DIR}/${STITCH_UNIVERSE}
