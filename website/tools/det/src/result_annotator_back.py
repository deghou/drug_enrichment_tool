from __future__ import division
import networkx as nx
import matplotlib.pyplot as plt
import os
from networkx import graphviz_layout
import operator
import numpy as np
from unittest.util import sorted_list_difference
import argparse

class GraphViz:
    
    def __init__(self):
        self.chemical_term_annotations = ""
        self.layout = {}
        self.layout_name = "default"
        self.networkxgraph = {}
        self.dictInteraction = {}
        self.chem_to_term_dic = {}
        self.term_to_chem_dic = {}
    
    ###
    ### SETTERS
    ###
    def setChemicalTermAnnotation(self,chemical_term_annotations):
        self.chemical_term_annotations = chemical_term_annotations

    def setLayout(self,layout):
        self.layout = layout   
    
    def setNetworkXGraph(self,networkxgraph):
        self.networkxgraph = networkxgraph 
    ###
    ### GETTERS
    ### 
    def getChemicalTermAnnoation(self):
        return self.chemical_term_annotations
    
    def getLayout(self):
        return self.layout

    def getNetworkXGraph(self,networkxgraph):
        return set.networkxgraph 

    def extractNodeConnectivity(self):
        #prepare graph
        chemicals = dict()
        terms = dict()
        nodeConnectivity = {}
        dictInteraction = {}
        for interaction in self.chemical_term_annotations:
            interaction = interaction.rstrip()
	    if len(interaction) == 2:
            	chemical = interaction.split("\t")[0]
            	term = interaction.split("\t")[1]
            	if chemical in chemicals:
            	    dictInteraction[chemical].append(term)
            	    chemicals[chemical] += 1
            	else:
            	    dictInteraction[chemical] = [term]
            	    chemicals[chemical] = 1
            	if term in terms:
            	    terms[term] += 1
            	else:
            	    terms[term] = 1
        nodeConnectivity['chemical_connectivity'] = chemicals
        nodeConnectivity['term_connectivity'] = terms
        nodeConnectivity
        ### Not very beautiful .. needs to be refactored
        self.dictInteraction = dictInteraction
        return nodeConnectivity
      
    def sortNodesPerConnectivityDegree(self):
        nodeConnectivity = self.extractNodeConnectivity()
        sortedNodes = {}
        #sort
        chemicals = nodeConnectivity["chemical_connectivity"] # dict
        terms = nodeConnectivity["term_connectivity"] # dict
        chemicals = sorted(chemicals.iteritems(), key=operator.itemgetter(1)) # list of tuples
        terms = sorted(terms.iteritems(), key=operator.itemgetter(1)) # list of tuples
        #add nodes
        chemicals_ordered = [(i[0]) for i in chemicals]
        terms_ordered = [(i[0]) for i in terms]
        sortedNodes["chemicals_sorted"] = chemicals_ordered
        sortedNodes["terms_sorted"] = terms_ordered
        return(sortedNodes) 

    def createGraph(self):
        #sort nodes per connectivity degree
        sortedNodes = self.sortNodesPerConnectivityDegree()
        chemicals_ordered = sortedNodes["chemicals_sorted"]
        terms_ordered = sortedNodes["terms_sorted"]
        #create graph
        G=nx.cycle_graph(0)
        G.add_nodes_from(chemicals_ordered)
        G.add_nodes_from(terms_ordered)
        #add edges
        for interaction in self.chemical_term_annotations:
            interaction = interaction.rstrip()
	    if len(interaction) == 2:
            	chemical = interaction.split("\t")[0]
            	term = interaction.split("\t")[1]
            	G.add_edge(chemical,term)
        return G
     
    def countCrossingEdges(self):
        edges_coordinate = []
        for edge in self.networkxgraph.edges():
            node_1,node_2 = edge
            node_drug = ''
            node_enri = ''
            if node_1.startswith('enrichment_term'):
                node_enri = node_1
                node_drug = node_2
            else:
                node_enri = node_2
                node_drug = node_1
            coord_x_drug,coord_y_drug = self.layout[node_drug]
            coord_x_enri,coord_y_enri = self.layout[node_enri]
            edges_coordinate.append([(coord_x_drug,coord_y_drug),(coord_x_enri,coord_y_enri)])
        crossing = 0
        edge_number = len(edges_coordinate)
        m = 0
        while m <= edge_number - 1:
            edge_m = edges_coordinate[m]
            n = m + 1
            while n <= edge_number - 1:
                edge_n = edges_coordinate[n]
                n += 1
                if self.areEdgesCrossing(edge_m,edge_n):
                    crossing += 1
            m += 1
        return crossing
        
    def areEdgesCrossing(self,edge_m,edge_n):
        x11,y11 = edge_m[0]
        x12,y12 = edge_m[1]
        x21,y21 = edge_n[0]
        x22,y22 = edge_n[1]
        if ((y11 > y21) and (y12 < y22)) or ((y11 < y21) and (y12 > y22)): 
            return True
        else:
            return False
                      
    def findLayout(self):
        #create layout
        layout_drugs = {}
        layout_terms = {}
        x = 2
        y = 0
        y_max = 100
        previous_number_of_crossings = -1        
        for term in self.term_to_chem_dic.viewkeys():
            y = y + y_max/len(self.term_to_chem_dic)
            layout_terms[term] = (x,y)
        iteration = 1
        while True:
            print "Iteration " + str(iteration)
            if iteration%2 == 0:
                dictInteraction = self.term_to_chem_dic
                nodes = self.term_to_chem_dic.viewkeys()
                layout_to_change = layout_terms
                reference_layout = layout_drugs
                new_x = 2
            else:
                dictInteraction = self.chem_to_term_dic
                nodes = self.chem_to_term_dic.viewkeys()
                layout_to_change = layout_drugs
                reference_layout = layout_terms
                new_x = 0
            x = 0 ; y = 0
            for node in nodes:
                partners = dictInteraction[node]
                y_coords_partners = []
                for part in partners:
                    x,y = reference_layout[part]
                    y_coords_partners.append(y)
                y_median = np.median(y_coords_partners)
                layout_to_change[node] = (new_x,y_median)
            sorted_layout_to_change = sorted(layout_to_change.iteritems(), key=operator.itemgetter(1))
            sorted_layout_to_change.reverse()
            nl = {}
            d_index = 0
            ### stretch chemical's relative position so that they take all the available space
            for node in sorted_layout_to_change:
                (x,y) = node[1]
                elem = node[0]
                y_stretched = (y + (y_max - y)) - ((y_max/len(sorted_layout_to_change)) * d_index)
                d_index += 1
                layout_to_change[node] = (x,y_stretched)
                nl[elem] = (x,y_stretched)
                if iteration%2 == 0:
                    layout_terms = nl
                else:
                    layout_drugs = nl
            layout = dict(reference_layout.items() + nl.items())
            self.layout = layout
            if self.countCrossingEdges() == previous_number_of_crossings:
                break
            previous_number_of_crossings = self.countCrossingEdges()
            print "Number of crossing edges " + str(self.countCrossingEdges())
            iteration += 1
            if iteration == 200:
                break
        return layout
                  
    def buildInteractionDics(self):
        chem_to_term_dic = {}
        term_to_chem_dic = {} 
        #print "Chemical term annotations : "
        print "\n"
        #print self.chemical_term_annotations
        for interaction in self.chemical_term_annotations:
            if len(interaction.split("\t")) == 2:
	    	chem = interaction.split("\t")[0]
            	term = interaction.split("\t")[1]
	    	print "chem : " + chem + " ; term : " + term
            	if chem in chem_to_term_dic:
            	    chem_to_term_dic[chem].append(term)
            	else:
            	    chem_to_term_dic[chem] = [term]
            	if term in term_to_chem_dic:
            	    term_to_chem_dic[term].append(chem)
            	else:
            	    term_to_chem_dic[term] = [chem]
        self.chem_to_term_dic = chem_to_term_dic
        self.term_to_chem_dic = term_to_chem_dic

    def drawGraph(self,network_output_file):
        #plot
        #draw nodes
        node_size = 150
        plt.figure(figsize=(12,12)) 
        label_font_size = 5
        for node in self.networkxgraph.nodes():            
            x,y=self.layout[node]
            if node.startswith("drug_"):
                nx.draw_networkx_nodes(self.networkxgraph,self.layout,node_size=node_size,nodelist=[node],node_shape = 'o',node_color = "white")                
                plt.text(x - 0.15,y,s=node.split("drug_")[1], horizontalalignment='center',fontsize = label_font_size)
            elif node.startswith('enrichment_term_'):
                nx.draw_networkx_nodes(self.networkxgraph,self.layout,node_size=node_size,nodelist=[node],node_shape = 'd', node_color = 'white')
                plt.text(x + 0.15,y,s=node.split("enrichment_term_")[1], horizontalalignment='center',fontsize = label_font_size)
        #draw edges
        color_edges = ["#000000","#0A9208","#C314D7","#0062E2","#FF0000","#82F74C","#FFBB0D","#5100FF"]
        color_i = 0
        drug_layout = {k:v for k,v in self.layout.iteritems() if k.startswith("drug_")}
        drug_layout_sorted = sorted(drug_layout.iteritems(), key=operator.itemgetter(1))
        drugs_sorted_by_y_coords = [str(i[0]) for i in drug_layout_sorted]
#        for node in self.chem_to_term_dic.viewkeys():
        for node in drugs_sorted_by_y_coords:
            edges = []            
            for partner in self.chem_to_term_dic[node]:
                edges.append((node,partner))
            nx.draw_networkx_edges(self.networkxgraph,self.layout,alpha = 0.5, edgelist = edges, edge_color = color_edges[color_i],width = 2)
            color_i += 1
            if color_i == len(color_edges):
                color_i = 0
        plt.axis('off')
	print network_output_file
        plt.savefig(network_output_file)
        #plt.show() 

    def updateNetwork(self,test):
        prefix = test.split("_")[0]
        for node in test.split("_")[1:]:
            self.networkxgraph = nx.relabel_nodes(self.networkxgraph,{"drug_" + node : test})
        
    def xCoordinatesToConcave(self):
        drug_layout = {k:v for k,v in self.layout.iteritems() if k.startswith("drug_")}
        drug_layout_sorted = sorted(drug_layout.iteritems(), key=operator.itemgetter(1))
        drugs_sorted_by_y_coords = [str(i[0]) for i in drug_layout_sorted]

        term_layout = {k:v for k,v in self.layout.iteritems() if k.startswith("enrichment_term_")}        
        term_layout_sorted = sorted(term_layout.iteritems(), key=operator.itemgetter(1))
        term_sorted_by_y_coords = [str(i[0]) for i in term_layout_sorted]
        distance = 20
        l = len(drug_layout_sorted)
        i = -1
        j = -1
        for d in drug_layout_sorted:
            drug,coordinates = d
            x,y = coordinates
            if j >= (l/2):
                i -= 1
                j += 1
            else:
                i += 1
                j += 1
            new_x = x - (i/distance)
            self.layout[drug] = (new_x,y)
        l = len(term_layout_sorted)
        i = -1
        j = -1
        for d in term_layout_sorted:
            term,coordinates = d
            x,y = coordinates
            if j >= (l/2):
                i -= 1
                j += 1
            else:
                i += 1
                j += 1
            new_x = x + (i/distance)
            self.layout[term] = (new_x,y)
        
            
            
            

                
    def mergeNodes(self):
        drugs = list(self.chem_to_term_dic.viewkeys())
        terms = list(self.term_to_chem_dic.viewkeys())
        drugs_dic_copy = self.chem_to_term_dic.copy()
        terms_dic_copy = self.term_to_chem_dic.copy()
        merged = {}
        m = 0
        ### first merge nodes
        while m < len(drugs) - 1:
            drug = drugs[m]
            print drug  
            n = m + 1
            drug_neighboor = self.chem_to_term_dic[drug]
            new_merged_name = "drug_" + drug.split('drug_')[1]
            while n < len(drugs):
                next_drug = drugs[n]
                next_drug_neighboor = self.chem_to_term_dic[next_drug] 
                ### do the drugs have the same neighboor ??
                if set(drug_neighboor) == set(next_drug_neighboor):
                    drugs_dic_copy.pop(next_drug,None)
                    n -= 1
                    drugs.remove(next_drug)
                    if drug in drugs_dic_copy:
                        drugs_dic_copy.pop(drug,None)
                    new_merged_name = new_merged_name  + "_" + next_drug.split("drug_")[1]
                n += 1
            ### if drugs have been merged then replace the nodes and labels in both dictionary
            if len(new_merged_name.split("_")) > 2:
                self.updateNetwork(new_merged_name)
                drugs_dic_copy[new_merged_name] = drug_neighboor
                for nb in drug_neighboor:
                    drugs_of_term = terms_dic_copy[nb]
                    i = 0
                    while i < len(drugs_of_term):
                        if drugs_of_term[i].split("_")[1] in new_merged_name:
                            drugs_of_term[i] = new_merged_name
                        i += 1
                    terms_dic_copy[nb] = list(set(drugs_of_term))
            m += 1
        self.chem_to_term_dic = drugs_dic_copy
        self.term_to_chem_dic = terms_dic_copy        



def __main__():
    # parse command-line arguments
    parser = argparse.ArgumentParser(description='Annotates result files from the enrichment tool with descriptions and links (generating HTML output)', version='0.1')
    parser.add_argument('-i', '--inputfile', type=str, help='file name of the enrichment output')
    parser.add_argument('-o', '--htmloutput', type=str, help='name of the output file to which html will be written')
    parser.add_argument('-a', '--annotation', type=str, help='foreground annotation')
    parser.add_argument('-c', '--cutoff', type=str, help='cutoff to filter out some edges')
    parser.add_argument('-on', '--output_network', type=str, help='name of the output file to which network will be printed (pdf)')
    parser.add_argument('--verbose', type=int, default=1, help='verbosity level')
    args = parser.parse_args()
    assert(args.inputfile is not None)
    assert(args.htmloutput is not None)
    args.output_network = args.output_network + ".png" 
    print "the inputed network output file is " + args.output_network
    print "the inputed html output file is " + args.htmloutput
    # TODO make this configurable
    descr_file = "/home/det/galaxy-dist/tools/det/databases/DB_all-drug-description.tsv"

    # TODO refactor a bit (break down into a dict initialization method and a selection method
    res = {}
    with open(descr_file,"r") as descr:
        for line in descr:
             items = line.strip().split('\t')
	     key, values = items[0], items[1:]
             res[key] = values
             
	h = open(args.htmloutput, 'w')
	#for all different type of table formats, check here: http://www.smashingmagazine.com/2008/08/13/top-10-css-table-designs/
	# I used 
	h.write('<!DOCTYPE html>\n<html>\n<head></head>\n<body>\n<style> #mytablestyle{font-family: "Arial", Sans-Serif; font-size: 12px; margin: 10px; text-align: left; border-collapse: collapse; border: 1px solid rgba(24,26,36,1);}\n#mytablestyle th{padding: 5px 10px; background: rgba(225,229,250,1); font-weight: 700; color: rgba(24,26,36,1);}\n#mytablestyle tbody{background: rgba(238,238,238,1);}\n#mytablestyle td{padding: 2px 10px; background: rgba(238,238,238,1); color: rgba(24,26,36,1); border-top: 1px dashed white;}\n#mytablestyle tbody tr:hover td{background: rgba(225,229,250,1);}\n</style>\n')
	h.write( '<table id="mytablestyle">\n')
	h.write( '<thead><tr><th>Drug Annotation</th><th>Description</th><th>P-value</th><th>Adj. P-value</th><th>Odds ratio</th><th>N</th></tr></thead>\n<tbody>\n')
    with open(args.inputfile,"r") as text:
        for line in text:
             items = line.strip().split('\t')
	     key, values = items[0], items[1:]
	     if key in res:
             	desc = res[key]
             #print key,'\t',desc[0]
             	h.write('<tr><td><a href="'+desc[1]+'" target="_blank">'+key+'</a></td><td>'+desc[0]+'</td><td>'+values[0]+'</td><td>'+values[1]+'</td><td>'+values[2]+'</td><td>'+values[3]+'</td></tr>')
    
    h.write('</tbody></table>\n</body>\n</html>')    
    
    ################################################
    ######## NETWORK VISUALIZATION START  ##########
    ################################################
    # read network
    network_tab = parseNetwork(args.inputfile,args.annotation,args.cutoff,"")
    chemical_term_annotations = network_tab.split('\n')
    print chemical_term_annotations
    ### Perform the following step if and only if the network has at least one edge
    if len(chemical_term_annotations) > 1:
	print "caca"
        graphViz = GraphViz()
        graphViz.setChemicalTermAnnotation(chemical_term_annotations)
        graphViz.buildInteractionDics()
        # create/set graph
        G = graphViz.createGraph()
        graphViz.setNetworkXGraph(G)
        # create/set layout
        graphViz.layout_name = "default"
        graphViz.mergeNodes()
        layout = graphViz.findLayout()
        graphViz.setLayout(layout)
    #    graphViz.xCoordinatesToConcave()
        # draw graph
        graphViz.drawGraph(args.output_network)
        h.write("<img src=" + args.output_network + " alt=\"network\">")
    else:
	print "pipi"
    ################################################
    ######## NETWORK VISUALIZATION STOP  ###########
    ################################################    
    h.close()

def parseNetwork(enrichment_results_file,annotation_file,cutoff,type):
    #build foreground dictionary (key chemical)
    foreground_annotation = {}
    network_tab = ""
    with open(annotation_file) as f:
        for line in f:
            (chemical, annotations) = line.split("\t")
            annotations = annotations.split(";")
            for a in annotations:
                a.rstrip()
                a.lstrip()
                if a in foreground_annotation:
                    foreground_annotation[a].append(chemical)
                else:
                    foreground_annotation[a] = [chemical]
    #Build network
    with open(enrichment_results_file) as f:
        next(f)
        for line in f:
            terms = line.split("\t")
	    enrichment_term = terms[0]
            p_value = terms[1]
            odds_ratio = terms[3]
            cut_off_respected = True
            if (type == "p-value" and p_value < cutoff) or (type == "odds" and odds_ratio < cutoff):
                cut_off_respected = True
            if cut_off_respected and enrichment_term in foreground_annotation:
                chemicals = foreground_annotation[enrichment_term]
                for ch in chemicals:
                    network_tab = network_tab + "drug_" + ch + "\tenrichment_term_" + enrichment_term + "\n"
        network_tab.rstrip()
    return(network_tab)
                
                
            

if __name__ == '__main__': __main__()


