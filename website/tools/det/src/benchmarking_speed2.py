from __future__ import division
from _pyio import open
from solr_controller import SolrController
import glob
import sys
import os
reload(sys)
sys.setdefaultencoding("utf-8")
import argparse
import time


### benchmark speed using randomly generated sets of chemical from pubchem universe and searched within stitch universe.
def benchmarkSpeed():
    files = '/home/det/det.0.1/test/benchmark-speed-10000-1-chemicals.txt'
    ddir = '/home/det/det.0.1/test/benchmark-speed-5000-1-chemicals.txt'
    print(ddir)
    files = glob.glob(os.path.abspath(ddir))
    times = []
    print(ddir)
    for namefile in files:
        start = time.time()
        namefile = files[0]
        outputfile = 'output.txt'
        sc = SolrController('tt',0.9,'names',2, search_space='stitch20141111')
        sc.matchNames(namefile, outputfile, True, True, True)
        end = time.time()
        times.append(end-start)
    print "\n\n\n"
    print times
     
    
if __name__ == '__main__':    
    benchmarkSpeed()
    
