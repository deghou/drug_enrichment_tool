The drug enrichment tool is a command line tool composed of 4 different modules:

[1]-Name matching module
	@INPUT : chemical names
	@OUTPUT : chemical names with fetched IDs
[2]-Synonyme module
	@INPUT : @OUTPUT from [1]
	@OUTPUT : @INPUT from [1] with their synonyms
[3]-Enrichment module
	@INPUT : @OUTPUT from [1]
	@OUTPUT : enrichment table
[4]-Results visualization module
	@INPUT : @OUTPUT from [3]
	@OUTPUT : network

They all function in the same way: they require an input file(s) and produce output file(s).
These modules have their own arguments and can be run with the command line independently from each other. 
The script `run-job.py` is a controller that streamlines [1] [3] and [4].
####################################################################################################################
#########################							EXAMPLE QUERIES						  ##########################
#################################################################################################################### 
###
### Perform name matching with one foreground and no background 
###
python run-job.py -n /g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/test/use-cases/zebrafish/cluster.17.tsv  -o output-foreground.txt --verbose 2
###
### Perform name matching with n (n=2) foregrounds and no background
###
python run-job.py -n /g/bork8/deghou/projects/enrichment_tool/demo/chemical_names_of_module_CODIM4.txt -n /g/bork8/deghou/projects/enrichment_tool/demo/chemical_names_of_module_CODIM2.txt  -o output-foreground.txt --verbose 2
###
### Perform name matching with n (n=2) foregrounds and one background
###
python run-job.py -n /g/bork8/deghou/projects/enrichment_tool/demo/chemical_names_of_module_CODIM2.txt -n /g/bork8/deghou/projects/enrichment_tool/demo/chemical_names_of_cancer_drug_sensitivity_drugs.txt -o output-foreground.txt -ob output-background.txt --verbose 2
###
### Perform name matching with one foreground and no background and enrichment with one database
###
python run-job.py -n /g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/test/use-cases/nsa/nsa-drugs-wiki.tsv -o output-foreground.txt -e true -oe output-enrichment.txt -d target_DrugBank --verbose 2
###
### Perform name matching with n (n=2) foregrounds and no background and enrichment with one database
###
python run-job.py -n /g/bork8/deghou/projects/enrichment_tool/demo/chemical_names_of_module_CODIM4.txt -n /g/bork8/deghou/projects/enrichment_tool/demo/chemical_names_of_module_CODIM2.txt  -o output-foreground.txt -e true -oe output-enrichment.txt -d 'target_STITCH' --verbose 2
###
### Perform name matching with n (n=2) foregrounds and no background and enrichment with m (m=2) databases
###
python run-job.py -n /g/bork8/deghou/projects/enrichment_tool/demo/chemical_names_of_module_CODIM2.txt -n /g/bork8/deghou/projects/enrichment_tool/demo/chemical_names_of_cancer_drug_sensitivity_drugs.txt -o output-foreground.txt -e true -oe output-enrichment.txt -d 'target_DrugBank' -d 'side-effect_SIDER' --verbose 2 
###
### Perform name matching with n (n=2) foregrounds and one background and enrichment with m (m=2) databases
###
python run-job.py -n /g/bork8/deghou/projects/enrichment_tool/demo/chemical_names_of_module_CODIM4.txt -n /g/bork8/deghou/projects/enrichment_tool/demo/chemical_names_of_module_CODIM2.txt  -o output-foreground.txt -b /g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/test/name-matching-enrichment-background-cmap.txt -ob output-background.txt -e true -oe output-enrichment.txt -d 'target_DrugBank' -d 'side-effect_SIDER' --verbose 2
###
### Perform name matching with n (n=2) foregrounds and one background and enrichment with ALL databases
###
python run-job.py -n /g/bork8/deghou/projects/enrichment_tool/demo/chemical_names_of_module_CODIM4.txt -n /g/bork8/deghou/projects/enrichment_tool/demo/chemical_names_of_module_CODIM2.txt  -o output-foreground.txt -b /g/bork8/deghou/projects/enrichment_tool/drug_enrichment_tool/test/name-matching-enrichment-background-cmap.txt -ob output-background.txt -e true -oe output-enrichment.txt -d ALL --verbose 2