# -*- encoding:ascii -*-
from mako import runtime, filters, cache
UNDEFINED = runtime.UNDEFINED
__M_dict_builtin = dict
__M_locals_builtin = locals
_magic_number = 6
_modified_time = 1422217479.243446
_template_filename='config/plugins/visualizations/scatterplot/templates/scatterplot.mako'
_template_uri='scatterplot.mako'
_template_cache=cache.Cache(__name__, _modified_time)
_source_encoding='ascii'
_exports = []


def render_body(context,**pageargs):
    context.caller_stack._push_frame()
    try:
        __M_locals = __M_dict_builtin(pageargs=pageargs)
        embedded = context.get('embedded', UNDEFINED)
        title = context.get('title', UNDEFINED)
        h = context.get('h', UNDEFINED)
        saved_visualization = context.get('saved_visualization', UNDEFINED)
        hda = context.get('hda', UNDEFINED)
        visualization_display_name = context.get('visualization_display_name', UNDEFINED)
        config = context.get('config', UNDEFINED)
        trans = context.get('trans', UNDEFINED)
        visualization_id = context.get('visualization_id', UNDEFINED)
        __M_writer = context.writer()
        # SOURCE LINE 1

        default_title = "Scatterplot of '" + hda.name + "'"
        info = hda.name
        if hda.info:
            info += ' : ' + hda.info
        
        # optionally bootstrap data from dprov
        ##data = list( hda.datatype.dataset_column_dataprovider( hda, limit=10000 ) )
        
        # Use root for resource loading.
        root = h.url_for( '/' )
        
        
        __M_locals_builtin_stored = __M_locals_builtin()
        __M_locals.update(__M_dict_builtin([(__M_key, __M_locals_builtin_stored[__M_key]) for __M_key in ['info','root','default_title'] if __M_key in __M_locals_builtin_stored]))
        # SOURCE LINE 12
        __M_writer(u'\n')
        # SOURCE LINE 14
        __M_writer(u'\n<!DOCTYPE HTML>\n<html>\n<head>\n<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />\n<title>')
        # SOURCE LINE 19
        __M_writer(unicode(title or default_title))
        __M_writer(u' | ')
        __M_writer(unicode(visualization_display_name))
        __M_writer(u'</title>\n\n')
        # SOURCE LINE 22
        __M_writer(unicode(h.css( 'base', 'jquery-ui/smoothness/jquery-ui')))
        __M_writer(u'\n')
        # SOURCE LINE 23
        __M_writer(unicode(h.stylesheet_link( root + 'plugins/visualizations/scatterplot/static/scatterplot.css' )))
        __M_writer(u'\n\n')
        # SOURCE LINE 26
        __M_writer(unicode(h.js( 'libs/jquery/jquery',
        'libs/jquery/jquery.migrate',
        'libs/jquery/jquery-ui',
        'libs/bootstrap',
        'libs/underscore',
        'libs/backbone/backbone',
        'libs/d3',
        'libs/handlebars.runtime',
        'mvc/ui',
        'jq-plugins/ui/peek-column-selector',
        'jq-plugins/ui/pagination',
        'mvc/visualization/visualization-model' )))
        # SOURCE LINE 37
        __M_writer(u'\n\n')
        # SOURCE LINE 39
        __M_writer(unicode(h.javascript_link( root + 'plugins/visualizations/scatterplot/static/scatterplot-edit.js' )))
        __M_writer(u'\n</head>\n\n')
        # SOURCE LINE 43
        __M_writer(u'<body>\n')
        # SOURCE LINE 44
        if embedded and saved_visualization:
            # SOURCE LINE 45
            __M_writer(u'<figcaption>\n    <span class="title">')
            # SOURCE LINE 46
            __M_writer(unicode(title))
            __M_writer(u'</span>\n    <span class="title-info">')
            # SOURCE LINE 47
            __M_writer(unicode(info))
            __M_writer(u'</span>\n</figcaption>\n<figure class="scatterplot-display"></div>\n\n<script type="text/javascript">\n$(function(){\n    var model = new ScatterplotModel({\n            id      : ')
            # SOURCE LINE 54
            __M_writer(unicode(h.dumps( visualization_id )))
            __M_writer(u' || undefined,\n            title   : "')
            # SOURCE LINE 55
            __M_writer(unicode(title))
            __M_writer(u'",\n            config  : ')
            # SOURCE LINE 56
            __M_writer(unicode(h.dumps( config, indent=2 )))
            __M_writer(u'\n        });\n        hdaJson = ')
            # SOURCE LINE 58
            __M_writer(unicode(h.dumps( trans.security.encode_dict_ids( hda.to_dict() ), indent=2 )))
            __M_writer(u",\n        display = new ScatterplotDisplay({\n            el      : $( '.scatterplot-display' ).attr( 'id', 'scatterplot-display-' + '")
            # SOURCE LINE 60
            __M_writer(unicode(visualization_id))
            __M_writer(u'\' ),\n            model   : model,\n            dataset : hdaJson,\n            embedded: "')
            # SOURCE LINE 63
            __M_writer(unicode(embedded))
            __M_writer(u'"\n        }).render();\n    display.fetchData();\n    //window.model = model;\n    //window.display = display;\n});\n\n</script>\n\n')
            # SOURCE LINE 72
        else:
            # SOURCE LINE 73
            __M_writer(u'<div class="chart-header">\n    <h2>')
            # SOURCE LINE 74
            __M_writer(unicode(title or default_title))
            __M_writer(u'</h2>\n    <p>')
            # SOURCE LINE 75
            __M_writer(unicode(info))
            __M_writer(u'</p>\n</div>\n\n<div class="scatterplot-editor"></div>\n<script type="text/javascript">\n$(function(){\n    var model   = new ScatterplotModel({\n            id      : ')
            # SOURCE LINE 82
            __M_writer(unicode(h.dumps( visualization_id )))
            __M_writer(u' || undefined,\n            title   : "')
            # SOURCE LINE 83
            __M_writer(unicode(title or default_title))
            __M_writer(u'",\n            config  : ')
            # SOURCE LINE 84
            __M_writer(unicode(h.dumps( config, indent=2 )))
            __M_writer(u'\n        }),\n        hdaJson = ')
            # SOURCE LINE 86
            __M_writer(unicode(h.dumps( trans.security.encode_dict_ids( hda.to_dict() ), indent=2 )))
            __M_writer(u",\n        editor  = new ScatterplotConfigEditor({\n            el      : $( '.scatterplot-editor' ).attr( 'id', 'scatterplot-editor-hda-' + hdaJson.id ),\n            model   : model,\n            dataset : hdaJson\n        }).render();\n    window.editor = editor;\n\n    $( '.chart-header h2' ).click( function(){\n        var returned = prompt( 'Enter a new title:' );\n        if( returned ){\n            model.set( 'title', returned );\n        }\n    });\n    model.on( 'change:title', function(){\n        $( '.chart-header h2' ).text( model.get( 'title' ) );\n        document.title = model.get( 'title' ) + ' | ' + '")
            # SOURCE LINE 102
            __M_writer(unicode(visualization_display_name))
            __M_writer(u"';\n    })\n});\n\n</script>\n")
            pass
        # SOURCE LINE 108
        __M_writer(u'\n</body>\n')
        return ''
    finally:
        context.caller_stack._pop_frame()


