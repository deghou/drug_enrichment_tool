#!/bin/bash
SOFTWARE_NAME=cart
SOLR_VERSION=solr
SOFTWARE_URL=http://cart.embl.de/static/cart.tar.gz
SOLR_INDEX_URL=http://cart.embl.de/static/${SOLR_VERSION}.tar.gz
DOWNLOAD_DIRECTORY=`pwd`
INSTALLATION_DIRECTORY=${HOME}
echo -n "Enter the absolute path of the desired installation directory and press [ENTER] (or type directly unter for an installation under ${HOME}): "
read INSTALLATION_DIRECTORY
if [ -z "$INSTALLATION_DIRECTORY" ]
then
INSTALLATION_DIRECTORY=${HOME}
INSTALLATION_DIRECTORY=${INSTALLATION_DIRECTORY}/drug-enrichment-tool
echo "CART will be installed here : ${INSTALLATION_DIRECTORY}"
else
INSTALLATION_DIRECTORY="${DOWNLOAD_DIRECTORY}/${INSTALLATION_DIRECTORY}"
echo "CART will be installed here : ${INSTALLATION_DIRECTORY}"
fi


SOLR_INSTALLATION_DIRECTORY=${INSTALLATION_DIRECTORY}/solr/solr
DATABASES_DIRECTORY=${INSTALLATION_DIRECTORY}/${SOFTWARE_NAME}/databases
DESCRIPTION_FILE="${INSTALLATION_DIRECTORY}/${SOFTWARE_NAME}/static/all-drug-description_ver2.txt"
HEADER_JS="${INSTALLATION_DIRECTORY}/${SOFTWARE_NAME}/static/header_js.js"
FOOTER_JS="${INSTALLATION_DIRECTORY}/${SOFTWARE_NAME}/static/footer_js.js"
TEST_DIRECTORY=${INSTALLATION_DIRECTORY}/${SOFTWARE_NAME}/"test"
PYTHON_VIRTUALENV=${SOFTWARE_NAME}-virtualenv
######################################################
########### USER'S PYTHON VERSIONAS TO BE = 2.7  #####
######################################################
case "$(python --version 2>&1)" in
    *" 2.7"*)
        echo "Python version requirements satisfied!"
        ;;
    *)
        echo "Wrong Python version! Please install python 2.7"
	exit
        ;;
esac
######################################################
########### SET THE PYTHON ENVIRONMENTS    ###########
######################################################
require_module=false
require_module_pandas=false
require_module_matplotlib=false
require_module_scipy=false
require_module_numpy=false
require_module_statsmodels=false
require_module_psutil=false
require_module_networkx=false
pip_installed=false
virtualenv_installed=false
virtualenv_created=false
python -c 'import pandas' 2>/dev/null && require_module_pandas=false || require_module_pandas=true
python -c 'import matplotlib' 2>/dev/null && require_module_matplotlib=false || require_module_matplotlib=true
python -c 'import scipy' 2>/dev/null && require_module_scipy=false || require_module_scipy=true
python -c 'import psutil' 2>/dev/null && require_module_psutil=false || require_module_psutil=true
python -c 'import numpy' 2>/dev/null && require_module_numpy=false || require_module_numpy=true
python -c 'import statsmodels' 2>/dev/null && require_module_statsmodels=false || require_module_statsmodels=true
python -c 'import networkx' 2>/dev/null && require_module_networkx=false || require_module_networkx=true
if which pip; then
    pip_installed=true
fi
if which virtualenv; then
    virtualenv_installed=true
fi
if $required_module_pandas || $required_module_matplotlib || $required_module_scipy || $required_module_numpy || $required_module_statsmodels || $required_module_psutil || $required_module_networkx; then
    echo "Your python installation requires at least one of these modules : pandas, matplotlib, scipy, psutil, numpy, statsmodels, networkx"
    echo "Shall CART install them for you (it might require the use of sudo, unless you have pip and virtualenv already installed) [y/n] ?"
    read sudo_authorization
    if [[ $sudo_authorization == "y" ]]; then
        if ! $virtualenv_installed; then
            if ! $pip_installed; then
                echo ""
                echo "CART will now install pip ..."
                sudo easy_install pip
                else
                    echo "No need to install pip (but virtualenv will start now)"
            fi
            echo "CART will now install virtualenv ..."
            sudo easy_install virutalenv
        fi
        echo "pip and virutalenv are installed"
        echo "CART will now create the python virtual environment (more about virtual environments : http://docs.python-guide.org/en/latest/dev/virtualenvs/)"
        virtualenv -p `which python` ${HOME}/.${PYTHON_VIRTUALENV}/python-2.7 --system-site-packages
        if [ -d "${HOME}/.${PYTHON_VIRTUALENV}/python-2.7/bin/" ]; then
            echo ""
            echo "CART has successfully created the python virtual environment"
            echo "The created environment encompasses all the python libraries you have installed in your system"
            virtualenv_created=true
        else
            echo ""
            echo "CART could not create the python virtual environment (should be here :${HOME}/.${PYTHON_VIRTUALENV}/python-2.7/bin/ )"
            exit
        fi
        
        if $virtualenv_created; then
            echo "CART will now activate the python virtual environment"
            echo "Important: the libraries installed by CART are only installed within the created environment, which leaves your original python installation as it was before you installed CART!!"
            echo "Important: before running CART, you will always need to activate the python virtual environment created by CART by typing this in a terminal prompt:"
            echo "source ${HOME}/.${PYTHON_VIRTUALENV}/python-2.7/bin/activate"
            echo ""
            echo ""
            echo ""
            echo "Continue [y/n] ?"
            read continue_authorization
            if [[ $continue_authorization == "y" ]]; then
                source ${HOME}/.${PYTHON_VIRTUALENV}/python-2.7/bin/activate
                if $require_module_pandas; then
                    pip install pandas
                fi
                if $require_module_matplotlib; then
                    pip install matplotlib
                fi
                if $require_module_scipy; then
                    pip install scipy
                fi
                if $require_module_numpy; then
                    pip install numpy
                fi
                if $require_module_statsmodels; then
                    pip install statsmodels
                fi
                if $require_module_psutil; then
                    pip install psutil
                fi
                if $require_module_networkx; then
                    pip install networkx
                fi
            else
                echo "Please install the required libraries yourself before downloading and configuring CART"
            fi
        fi
    else
        echo "Please install the required libraries before installing det"
        exit
    fi
else
 echo "no module required"
fi
######################################################
########### USER'S JAVA VERSION HAS TO BE > 1.5  #####
######################################################
if type -p java; then
    echo found java executable in PATH
    _java=java
elif [[ -n "$JAVA_HOME" ]] && [[ -x "$JAVA_HOME/bin/java" ]];  then
    echo found java executable in JAVA_HOME
    _java="$JAVA_HOME/bin/java"
else
    echo "It looks like java is not installed. Please install java (version 1.6 or higher) and re-run this script."
    exit
fi

if [[ "$_java" ]]; then
    version=$("$_java" -version 2>&1 | awk -F '"' '/version/ {print $2}')
    echo version "$version"
    if [[ "$version" > "1.5" ]]; then
        echo "Java version requirement is satisfied( > 1.5)"
    else
        echo "Java version is less than 1.5, please install a new java version before pursuing (java > 1.5 is required to run the name matching tool :-)"
	exit 
    fi
fi

######################################################
########### GET THE TOOL AND THE RESOURCES ###########
######################################################
###CREATE A FOLDER
if [ ! -d "${INSTALLATION_DIRECTORY}" ]; then
    mkdir ${INSTALLATION_DIRECTORY}
fi
cd ${INSTALLATION_DIRECTORY}
### Download tool
curl ${SOFTWARE_URL} > ${SOFTWARE_NAME}.tar.gz
tar -xvzf ${SOFTWARE_NAME}.tar.gz
### Download solr
curl ${SOLR_INDEX_URL} > ${SOLR_URL}.tar.gz
tar -xvzf ${SOLR_URL}.tar.gz


######################################################
########### SET THE CONFIG FILE (PATHS etc)###########
######################################################
echo "[solr]
solr_install_dir = ${SOLR_INSTALLATION_DIRECTORY}
# usually, just setting this to 'java' should work fine
jre_cmd = `which java`
jre_mem = 8000m
stop_key = 8c8c8c
# list will be split into words at characters other than the set of alphanumeric ones
# expanded by '.', '-' and '_'
search_indices = pubchem-filtered20141110
                 pubchem-unfiltered20141110
                 stitch20141111

[visualization]
description_file = ${DESCRIPTION_FILE}
header_js = ${HEADER_JS}
footer_js = ${FOOTER_JS}

[annotation]
db_dir = ${DATABASES_DIRECTORY}
db_format = tsv
db_prefix = DB
# list will be split into words at characters other than the set of alphanumeric ones
# expanded by '.', '-' and '_' (so these are also allowed in corresponding file names)
supp_methods = Fisher, ROC
supp_databases = all-drug-description, drug-ATC-code-L4, drugbank-targets-action, STITCH-drug-targets,
                 chembl-ftc-drug-terms, drug-ATC-code, drug-side-effects, TTD-targets-Activation,
                 chemicals_chebi, drugbank-metabolization-action, sider-indications, TTD-targets-Inhibition,
                 drug-ATC-code-L2, drugbank-targets-action-Activation, STITCH-drug-targets-Activation, TTD-targets,
                 drug-ATC-code-L3, drugbank-targets-action-Inhibition, STITCH-drug-targets-Inhibition," > ${SOFTWARE_NAME}/conf/settings.cfg
#####################################################
###########        RUNNING THE TEST        ###########
######################################################
source ${HOME}/.${PYTHON_VIRTUALENV}/python-2.7/bin/activate
echo "######## IMPORTANT ########"
echo "To ensure that you always use the right python version when you run ${SOFTWARE_NAME}, always first run the following command before :"
echo "source ${HOME}/.${PYTHON_VIRTUALENV}/python-2.7/bin/activate"
echo "######## RUN TEST FILE (all at once mode) ########"
echo "cd ${INSTALLATION_DIRECTORY}/${SOFTWARE_NAME}/test"
echo "bash RUN_EXAMPLE.sh"
echo "######## IMPORTANT ########"
echo "To delete CART, simply run this :"
echo "rm -r ${INSTALLATION_DIRECTORY}"
