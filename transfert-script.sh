### This script extracts the cart content of
### the tool running within galaxy and pack it
### into a folder ready to be used for the command
### command line version of cart

### To be executed here:
### ssh det@sam
### cd ~
### bash transfert-script.sh

cp -r galaxy-dist-v2/tools/det ./
rm -rf cart/
mv det cart
cd cart
rm *xml
rm *~
rm Gala*
rm setup.py
rm -rf example_out
rm -rf Distutil*
rm -rf dist
rm -rf database-v.27.09.2015
rm -rf database-v.13.11.2015
rm -rf databases/DB-backup-15.05.15
cp src/*py ./
cp src/*java ./
cp src/*class ./
cp src/*jar ./
rm -rf src/*
mv *py src
rm Hello*
rm TestMain*
mv *java src
mv *class src
mv *jar src
rm src/bench*
rm src/annotation-summary-table.py
rm src/*back*
rm src/results_visualization.py
rm -rf git-embl/
rm -rf lib/
rm LICENCE
rm -rf databases/for-stats/
mv test/RUN_EXAMPLE.sh ./
mv test/cmap_background.tsv ./
mv test/cmap_foreground_codim2.tsv ./
rm -r test/*
mv RUN_EXAMPLE.sh test/
mv cmap* test/
rm -rf res/
rm -rf tmp/
cp ../settings-for-users.cfg conf/settings.cfg
cd ../
tar -zcvf cart.tar.gz cart
cp cart.tar.gz galaxy-dist-v2/static/

